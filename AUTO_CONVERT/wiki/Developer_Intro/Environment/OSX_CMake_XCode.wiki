See [[Building Blender/Mac|Building with XCode]]

The text below is out of date except [[Source/Code_Style/Configuration#Xcode|Configuring The Editor]].

== Starting out ==
* This document assumes you have built blender using the [[Building_Blender/Mac|Building Blender for OSX]].
* Tested with Xcode 4.6.2 (4H1003) on Mac OS X 10.8.5

== Import Project ==

* Start Xcode
* Click on "Open Other..." button in the left bottom corner of the splash screen
* Navigate to created by CMake project folder
* Select "Blender.xcodeproj" file and click "Open" button

== Building Project ==

* Select "install" goal
* Click "Run" and all projects will be build

== Launching Blender ==

* Select "blender" goal
* Click "Run"

== Configuring The Editor ==
See: [[Source/Code_Style/Configuration#Xcode]]