== Starting out ==
* This document assumes you have built blender using the [[Building_Blender/Linux/Ubuntu/CMake|Linux/CMake Quickstart]].
* Tested with NetBeans 7.4

* Blender uses a lot of memory in netbeans, for much faster parsing start netbeans with the argument: '''-J-Xmx1024m'''
* For smooth text display you can use this argument: '''-J-Dawt.useSystemAAFontSettings=lcd'''
* ''You may need to enable the C/C++ plug-in from within NetBeans (it may not be on by default)''
** Tools (Menu) -> Plugins
** Settings (Tab) -> check Netbeans Distribution
** Available Plugins (Tab) -> check C/C++ and install it

== Project File ==

To create a project file for netbeans run

 python3 BLENDER_SOURCE/build_files/cmake/cmake_netbeans_project.py BLENDER_BUILD

... where '''BLENDER_SOURCE''' Is blenders source directory and '''BLENDER_BUILD''' is the cmake build directory containing CMakeCache.txt

''you may want to use a bash alias or shell script for this command.''

To load the project file:
* File (Menu) -> Open Project
* Navigate to the CMake Build directory, double clicking on it will load it.<br>''When loading netbeans in the future this project will be available in the welcome screen''

* netbeans will ask if it should assosiate 'inl' with C/C++ header<br>Click yes.


* NOTE: For an unknown reason code assistance is not parsing Blender's code on loading the project file.<br>This can be worked around by right clicking on the 'Blender' project icon and selecting '''Code Assistance -> Reparse Project''' (netbeans 8.0.2, --[[User:Ideasman42|Ideasman42]] 00:08, 18 June 2015 (UTC)).

== Launching Blender ==

* File (menu) -> Project Properties - Blender
** ''Run''
** Set 'Run Directory' to your blender source-code path.
** Disable 'Build First'

== Configuring The Editor ==
See: [[Source/Code_Style/Configuration#NetBeans]]