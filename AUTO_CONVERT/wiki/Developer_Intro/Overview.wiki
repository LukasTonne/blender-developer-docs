=New Developer Info=


<div class="bd-lead">

This page is meant to provide new developers with information about how the Blender development process works, where to find info and how to find a good project to work on.

</div>

== Communication ==

There are various communication channels used.

* [https://projects.blender.org/ '''Issue Tracker'''] - for bugs, code contributions and managing design tasks.
* [https://blender.chat/ '''Blender Chat'''] - [https://blender.chat/channel/blender-coders #blender-coders] for informal conversation and discussions with/between developers.
* [https://devtalk.blender.org '''DevTalk Forum'''] - for general developer discussion & questions.

See the [[Contact|Contact]] page for more information.

If you want to start developing in Blender you will have lots of startup questions. You can ask them in the #blender-coders blender.chat channel each day of the week.

When in doubt and no particular channel seems to match your question, write a message on #blender-coders and you will get an answer there or get redirected to the right channel.

No spamming or flaming is allowed on our platforms. Please stay on topic and follow good net etiquette.

== Module Meetings ==

The modules have autonomy to organize their work how they see fit. Some modules have weekly or bi-weekly meetings. The less active modules meet once there is something specific to discuss. The future meetings are announced as part of the [https://devtalk.blender.org/tag/meeting meeting notes].

== Getting Started ==

==== Building Blender ====

See [[Building Blender]].

==== Decisions ====

Blender development is structured similarly to Python's BDFL model (BDFL stands for [http://en.wikipedia.org/wiki/Benevolent_Dictator_for_Life Benevolent Dictator For Life]). The idea is that the project is managed by one person, in this case Ton Roosendaal, who delegates design decisions to a group of [[Modules|module owners]], while still maintaining a veto right of what goes into Blender.

==== Your First Project ====

Once you have blender compiled you're probably wondering where to start tinkering. If you are a Blender user then there is probably some niggling thing you would like to add or change. If you are new to Blender then you will need to take a look and see what you might work on.

There are no hard and fast rules about where to start, so find an area that interests you and start looking onto it.

A good location to start checking on what is on the roadmaps for modules is the list on the front page of [https://projects.blender.org/ projects.blender.org].

Some hints for your first projects:
* An area of interest (even better, an area of Blender you use)
* Double check it is not already done.
* Try to avoid a project that spans across many files and areas of Blender, since you may get bogged down trying to understand everything at once.

Here are some areas that can give you a foot in the door:

===== Small Features =====

Adding in small features is a good way to start getting your hands dirty, even if you don't contribute the changes. The feature you worked on can evolve into something more useful, take a new direction, or spark interest in new areas to develop.

[https://projects.blender.org/blender/blender/issues?labels=302 Here is a list of quick hacks] - tasks that core developers think could be reasonably tackled as a first blender project without a major time commitment (these tasks typically would take a core developer less than 4 hours to accomplish, but might take quite a bit more time for a new coder who needs to learn the code base).

===== Bug Fixing =====

One of the easiest ways to get involved with coding Blender is by fixing bugs. Bug-fixing lets you focus on a small part of the Blender source rather than trying to understand it all at once. The list of current bugs is on [http://projects.blender.org projects.blender.org]. So pick an area of interest and start fixing! When you have got it, [[Tools/CodeReview|make a pull request]].

==Navigating the Code==

Have a look at the [[Source/File_Structure|Files structure]] and [http://www.blender.org/bf/codelayout.jpg Code Layout] diagram.

The editors directory is usually a good place - it is where most of the operators live. Have a look at the header files and structs related to what you are interested in working on. The headers usually have the best overview of what a function does. To find the struct a simple grep or other search for struct Foo.

You can also start with writing python scripts, the [https://docs.blender.org/api/master/ API for our python tools] - is similar in many ways to our C API. You can often find out where some C code lives by seeing the python tool tips by hovering over a button and seeing what the operator name is. If you add a console window you can see what is output to it when you do an action, then just search the code.

Also putting a break on a function in a debugger and doing a back trace can help you find the path code took to get to your function of interest. Or you can start blender from the command line with the -d option and every command is printed to the console.

==Debugging==

Have a look at [[Tools/Debugging|Debugging]] for help and hints on doing debugging. If you end up not being able to solve the bug yourself, post a bug to our [https://projects.blender.org/blender/Blender/issues Bug Tracker], if it is a bug in our code.

If it is a bug in your code, load your test file and the information you have found out about the bug to [http://www.pasteall.org pasteall.org] then ask for help in [https://blender.chat/channel/blender-coders #blender-coders] on blender.chat. 

== Development Process ==

Steps for getting a change into Blender
# Find an area in Blender you want to improve upon.
# If it's a non-trivial feature, check in advance if Blender developers agree with the project and design, to avoid doing work for nothing. Contacting developers is typically done on the [https://devtalk.blender.org developer forums], or on [https://blender.chat/channel/blender-coders #blender-coders] on blender.chat. To talk to the individual owners of parts of the code, see the list of [[Modules|module owners]]
# Do the actual coding :)
# Once you have code, see the [[Process/Contributing_Code|Contributing Code]] section on how to [[Tools/CodeReview|make a pull request]] get it submitted, reviewed and included.

== Technical Documentation ==

We encourage everyone to use the [http://wiki.blender.org Blender Wiki] to document code work and to share proposals. Note that we strictly apply the rule "No code gets in trunk without documentation!". Also never hesitate to include good example .blend files and tests and videos showing the feature.

People who only want to help with technical documentation are welcome too. Just use the general contact information here to get started.

== Release Process ==

For more information about the release schedule, see [[Process/Release_Cycle|Release Cycle]] and [[Dev:Projects|Current Projects]] to understand the schedule and times when new features can get in and releases are made.