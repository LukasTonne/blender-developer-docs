=Python Style Guide=

Python code should adhere to [https://www.python.org/dev/peps/pep-0008/ PEP 8], with the following clarifications:

* Only use four spaces for indentation, no tabs.
* Use Unix-style end of line (`LF`, aka `'\n'` character).
* Spaces around operators (except for keyword arguments).
* Use <tt>CamelCase</tt> for classes and exception types. Use <tt>underscore_case</tt> for everything else.

== Automated Formatting ==

Most Python code is automatically formatted using [https://pypi.org/project/autopep8 autopep8].

Use `make format` to format both C/C++ and Python code in the entire repository, or integrate it in your IDE.

== Additions to PEP 8 ==

=== Naming ===

* Avoid shadowing names of Python built-in [https://docs.python.org/3/library/functions.html functions], [https://docs.python.org/3/library/constants.html constants], [https://docs.python.org/3/library/stdtypes.html types], and [https://docs.python.org/3/library/exceptions.html exceptions]. Instead of using `object`, use `obj`; as for other names, the Python built-ins are very generic, so often it is better to be more specific in your naming. For example, instead of naming a list `list`, name it `objects_to_export`. Not only does this avoid shadowing a built-in name, it also is more explicit and emphasises the contents of the list. If you want to make the type of the variable explicit, use [https://docs.python.org/3/library/typing.html type annotations].
* Avoid overly short names. Use `mesh` and `curve` instead of `me` and `cu` (or worse, `m` and `c`).

=== Unused Variables & Arguments ===

It's sometimes necessary to declare variables or arguments that aren't used, in this case it's a common convention to begin the variable with an underscore.

Tools such as pylint will skip them when reporting unused variables.

For example, a menu's draw method always takes a context argument, which you don't necessarily need.

<syntaxhighlight lang="Python">
class EXAMPLE_MT_menu(Menu):
    bl_label = ""

    def draw(self, _context):
        layout = self.layout
        layout.menu("EXAMPLE_MT_submenu_a")
        layout.menu("EXAMPLE_MT_submenu_b")
</syntaxhighlight>

Or when iterating over a sequence some items may not be used.

<syntaxhighlight lang="Python">
for (vert, _edge, face) in elements:
    process(vert, face)
</syntaxhighlight>

== Exceptions to PEP 8 ==

* Line width:<br>Maximum width for lines is 120 for all scripts.
* Imports:<br>We often put imports within functions body which is '''not''' pep8 compliant.<br>This is done to speed up Blender's startup times to avoid loading in many files and libraries which the user may never access.

== Conventions for Core Scripts ==

These scripts run as part of Blender at startup (`scripts/startup` and scripts in `scripts/modules` that run on startup).
There are some additional conventions for these scripts.

* Postpone importing Python modules, where imports may be in a function or a method body.
:
: This is done for faster startup times and avoid unnecessary overhead when running tests or rendering in background mode.

* No type annotations (besides those necessary for `bpy.props` use).
: Currently we don't have a practical way to validate these, see [https://developer.blender.org/T87333 this task] for details.

* Use percentage string formatting (no f-string of `str.format(..)` usage).
:
: This is done because percentage formatting works well for string literals that need to be translated into other languages and it keeps the code-base simpler. While there are advantages to alternative string-formatting methods, core-scripts typically perform primitive operations, so the advantages to using other methods isn't significant, (see: [https://devtalk.blender.org/t/python-string-formatting/14040 Python String Formatting] discussion).

* Single quotes for enumerator literals, double quotes for everything else.
:
: Use single quotes for enums in checks such as `ob.type == 'MESH'`, double quotes for other strings such as `layout.label(text="Label Text")`.

== Related Topics ==

* See: [[Tools/Blender_Tools_Repo|Blender's Dev Tools repo (includes style checker)]]
* See: [[Source/Code_Style/Configuration|Configuration for Blenders Code Style (IDE's, Editors & Formatting Tools)]]