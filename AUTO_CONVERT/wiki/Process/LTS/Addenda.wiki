== Timeline ==

* ''February 29th, 2020'' - [https://code.blender.org/2020/02/release-planning-2020-2025/ Initial announcement]
* ''May 5th, 2020'' - [https://code.blender.org/2020/05/long-term-support-pilot/ Pilot program proposal]

== Issue severity levels ==

Severity 1, 2, 3, 4 maps roughly to:

# Unbreak now! and high priority bugs
# High priority and Normal bugs
# Normal, medium, low priority bugs
# Features

== Open Issues ==

LTS also introduces a potential confusion with multiple Blender versions co-existing. This problem is not unique to LTS, but aggravated here:

* The communication is to focus in the latest version, with a hint to the upcoming beta release. This reaches most of the users.
* Early adopters and bug reporters are directed to the daily builds, focusing mostly on master.
* LTS members get their Blender from a different page / update system. Bugs reported in an LTS version are treated as any other bug, and if already fixed in master are considered closed.