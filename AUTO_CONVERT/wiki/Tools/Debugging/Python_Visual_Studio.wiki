= Debugging blender python with Visual Studio =

== Update 2023-01 ==

Visual studio breaks this feature often, it's best to consider this feature non functional and not sink any time into trying to get it to work.

The page below is archived in its original state in case a working version ships in a future Visual Studio version.

Relevant upstream ticket:

https://github.com/microsoft/PTVS/issues/6712

== Introduction ==

Visual Studio 2017 offers integrated debugging of python. This guide will help you set it up for use with blender.

This guide assumes you already know how to build blender, if you have not yet done so, please follow the [[Building_Blender/Windows]] guide.

== Prerequisites ==

=== Visual Studio 2017 === 

Version 15.8.7 or later.

* In the visual studio installer, enable the `Python Development` workload

  [[File:Vs python installer.png]]

=== Visual Studio 2019 ===

* Enable the Python Development Workload (just like for VS2017)
* Enable the "Python native development tools" individual component. This is not enabled by default with Python Development Workload, but is required for the "Python/Native Debugger" option to show up later.

== Project Creation ==

You should already have used `make.bat` to build blender before, in this section we'll use it to generate a Visual Studio project with the options enabled we need for debugging python. 

Open up a command prompt and navigate to the blender source folder and run <tt><b>make full 2017 x64 nobuild pydebug</b></tt>

<syntaxhighlight lang="Python" start=1 line >
Microsoft Windows [Version 6.1.7601]
Copyright (c) 2009 Microsoft Corporation.  All rights reserved.

C:\Windows\system32>cd /d k:\BlenderGit\blender

k:\BlenderGit\blender>make 2017 x64 nobuild pydebug
</syntaxhighlight>


{|class="note"
|-
|<div class="note_title">'''Tip:'''</div>
<div class="note_content">
Here's what each of the additional parameters mean.
  
* <tt><b>2017</b></tt> This explicitly select Visual Studio 2017 in case there are multiple versions of Visual Studio installed.  
  
* <tt><b>x64</b></tt> This explicitly selects a 64 bit build of blender, while 32 bit builds are still supported (use `x86` for that) it is not recommended unless you really need a 32 bit build.  
  
* <tt><b>nobuild</b></tt> This prevents `make.bat` from building blender for you, and will only generate a Visual Studio solution for you that you can later open with the IDE.  
  
* <tt><b>pydebug</b></tt> This sets up the project for python debugging and will cause CMAKE to include all .py scripts from the release folder and all users scripts in sub-projects in the solution.  
</div>
|}


After a few seconds, it should tell you where the project files have been written.

<syntaxhighlight lang="Python" highlight=3 start=1 line >
-- Configuring done
-- Generating done
-- Build files have been written to: K:/BlenderGit/build_windows_Full_x64_vc15_Release
</syntaxhighlight>


== Opening the project in visual studio ==

Navigate to the location and double click <tt><b>blender.sln</b></tt>

== Selecting a configuration ==

You can build blender in both <tt><b>Debug</b></tt> and <tt><b>Release</b></tt> configurations. 

{|class="note"
|-
|<div class="note_title">'''Tip:'''</div>
<div class="note_content">
<tt><b>Debug</b></tt> is a specially optimized build for debugging code, this build of blender will be bigger and slower, but it will be easier to debug C/C++ code.  

<tt><b>Release</b></tt> is a highly optimized version of blender, it will be fast, but it will be problematic to diagnose issues in the C/C++ Code.
</div>
|}


You can switch between the builds in the <tt><b>Solution Configuration</b></tt> dropdown menu

[[File:Vs configuration dropdown.png]]

{|class="note"
|-
|<div class="note_title">'''Tip:'''</div>
<div class="note_content">
If you are only going to debug python code select the <tt><b>Release</b></tt> configuration, if you also need C/C++ debugging select <tt><b>Debug</b></tt>
</div>
|}


== Building Blender ==

First we have to build blender, however just building blender is not enough, the output folder also needs to be populated with additional dll's and startup scripts for blender to run properly, so instead of just clicking build. Expand the <tt><b>CMakePredefinedTargets</b></tt> group , right click on the <tt><b>INSTALL</b></tt> project and select <tt><b>build</b></tt>

[[File:Vs blender build.png]]

after a while the output window should show something along these lines

<syntaxhighlight lang="Python" start=1 line >
========== Build: 149 succeeded, 0 failed, 0 up-to-date, 0 skipped ==========
</syntaxhighlight>


== Selecting the Python Debugger == 

Select <tt><b>Python/Native Debugger</b></tt> in the debugger dropdown.

[[File:Vs debugger selection.png]]

{|class="note"
|-
|<div class="note_title">'''Tip:'''</div>
<div class="note_content">
If this option is missing, this is most likely due to the <tt><b>Python Development</b></tt> workload not being installed, see the Prerequisites section on how to install.
</div>
|}


== Setting a breakpoint ==

In the solution explorer, expand <tt><b>scripts/blender_python_system_scripts/addons/io_scene_obj</b></tt>, double click on <tt><b>__init__.py</b></tt>, and put a breakpoint on the register function by placing the cursor on the following line and pressing <tt><b>F9</b></tt> or by clicking in the left margin of the line. 

<syntaxhighlight lang="Python" highlight=2 start=329 line>
def register():
    for cls in classes:
        bpy.utils.register_class(cls)

    bpy.types.TOPBAR_MT_file_import.append(menu_func_import)
    bpy.types.TOPBAR_MT_file_export.append(menu_func_export)
</syntaxhighlight>

== Running blender ==

Start blender by either pressing the <tt><b>F5</b></tt> key or clicking the play [[File:Vs play.png]] button.

Blender will now start and after a few moments your breakpoint should hit.

Happy Debugging!

[[File:Vs blender breakpoint.png|200px|thumb||left]]