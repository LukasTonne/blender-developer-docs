== Note for editors ==

It might seem that editing a certain distro's page won't affect other distros pages, but this is not the case: the pages above are based on:

* [[Template:Building Blender/Linux/intro|Intro]]
* [[Template:Building Blender/Linux/dependencies|Dependencies]]
* [[Template:Building Blender/Linux/cmake|CMake]]
* [[Template:Building Blender/Linux/Troubleshooting|Troubleshooting]]