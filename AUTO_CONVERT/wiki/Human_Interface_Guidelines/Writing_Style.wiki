= Writing Style =

''(Guideline Status: Approved.)''

=== Common Rules ===

* '''Use American English.'''
* '''Keep text short and to the point.'''
* '''Do not use abbreviations like “verts”, “VGroups”, "loc", or "rot", "fac"'''. Instead, use plain words like “vertices”, “vertex groups”, "location", "rotation", "factor".
* '''Do not use English contractions like “aren’t”, “can’t”, etc.''' It's preferred to keep full spelling, “are not” or “cannot” are not that much longer, and it helps keeping consistent styling over the whole UI.
* '''Capitalize titles.''' Entity names are not titles.
** Do '''not''' capitalize "mesh", "object", "vertex group", etc.
** Not capitalizing entity names is an arguable guideline, but avoids many fuzzy cases.
* '''“Channel” identifiers, like X, Y, Z, R, G, B, etc. are always capitalized.'''
* '''Do not use implementation specific language.'''
** Use "data-block", '''not''' "ID".
** Do '''not''' use terms like "flag", "string", "RNA", "DNA".
* '''Don't use pronouns, such as "''you''".''' This sounds like an accusation towards the user.<br/>'' '''Don't''': "Your mesh is very dense"<br/>'''Do''': "The mesh is very dense"''
* '''Use the ampersand ("&") in labels, but "and" in text (e.g. tooltip descriptions).'''
* '''Text should generally <u>not</u> contain:'''
** Code snippets.
** Anything with very involved details (e.g. troubleshooting, corner cases that might not work, etc.).

Formatting with new lines (`\n`) and bullet points (`\u2022`) is fine.

=== UI Labels ===

* '''Must use English MLA title case''' (see [https://titlecaseconverter.com/rules/#MLA this overview], also [http://developer.blender.org/T79589 #79589]).
** Tip: There are various online converters available.
** MLA title case examples:
*** Like in This Example
*** Set Offset from Cursor
*** Alexander and the Terrible, Horrible, No Good, Very Bad Day
*** Be Careful What You Wish For
* '''An operation that opens a new popup or window should end with an ellipsis ("...").'''
* '''Avoid redundancy.''' E.g. do '''not''' use "Enable", "Activate", "Is", "Use", or similar words for boolean properties (e.g. labels in checkboxes).
** '''Don't''': ''"Use Transparency"''
** '''Do''': ''"Transparency"''