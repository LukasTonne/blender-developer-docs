= OpenXR-SDK Dependency =

Notes about maintaining the OpenXR-SDK library dependency.

== Updating to new Version ==

Repository: https://github.com/KhronosGroup/OpenXR-SDK<br/>
Note: Use the OpenXR-SDK repository, NOT the OpenXR-SDK-Source one!

* `build_files/build_environment/cmake/xr_openxr.cmake`: may require version number changes
* `build_files/build_environment/cmake/versions.cmake`: update `XR_OPENXR_SDK_VERSION` and `XR_OPENXR_SDK_HASH` (MD5 of .tar.gz)
* `build_files/build_environment/install_deps.sh`: update `XR_OPENXR_VERSION` and `XR_OPENXR_REPO_UID` (commit hash)
* `build_files/cmake/platform_win32.cmake`: may require version number changes

Remember to always check the specification's change log for compatibility breaking changes.