= Asset Bundles =

Starting in Blender 3.0 separate downloads are available for asset bundles. Those bundles are an experiment that will be iterated over during the 3.x series.

== Download ==

Individual Bundles:
<div style="width:auto;margin:0px;overflow:auto;">
{| width="100%" style="font-size:90%; background:transparent;"
| [[:Special:Prefixindex/Source/Architecture/Asset System/Asset Bundles/]]
|}</div>

== Definition ==

* Building blocks
* Quick scene draft and layout
* Start point to customization
* Ready to play

== X-Ray ==

Each asset bundle is an independent .zip package that can be download and used independently. It contains:

* `bundle_file.blend`
* `blender_assets.cats.txt`

The assets (.blend) file contains the assets, while also serving as a demo scene to test the assets. The file can also be used as a base to expand the bundle and customize the existing assets. The catalog file (.cats.txt) is used to organize the assets in different catalogs.

== Installing ==

The bundle files can be installed from with-in Blender. If the files are changed (e.g., more assets are created) they need to still be self-contained (not depend on external dependencies) to be able to be installed.