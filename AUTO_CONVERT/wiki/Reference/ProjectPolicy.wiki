<blockquote>
Hi all,

Blender's growing acceptance in the professional market is much  
rewarding and a great compliment for everyone who contributed to it!  
Success is a double-edged sword though... we can't become competitive  
for commercial products without getting competitors to react to this.   
Although a proper market-mechanism would just mean they'll improve  
their offerings, there's also the more ugly side of competition  
related to legal actions around IP and patents.

I really cherish the very relaxed and informal development projects we  
conduct. This loosely regulated chaos also makes us vulnerable though.  
It's about time for everyone to more closely watch what you write in  
public and how to publish work you do on Blender. Most relevant is to  
not give out invitations to attract unwanted attention, but especially  
to cast the attention to Blender Foundation, GNU GPL and the Free  
Software and Open Source movement in general.

For all Blender developers, I'd like to propose the following  
guidelines for their docs/work:

1) For everyone in general:

On own blogs:
- Mention GNU GPL (or other OS licenses) when you talk about your code  
work.
- Credit Blender or blender.org on your site where appropriate.

Everywhere:
- Only link to copyrighted docs on web if you have the (c) owner  
permission.
- Stay away from mentioning commercial packages in user/tech docs ever.
- Document work in ways it shows the design process and ideas you  
explored

2) Highly advised, especially for competitive work:

- Publicly present your work as endorsed or commissioned by Blender  
Founation
- Present yourself as "member of the team working on Blender".
- Use (also) our code.blender.org blog for articles, and  
wiki.blender.org for design docs
- Add videos to the BF vimeo or youtube account

3) Optional, for best protection:

- Commit under BF copyright. You can use a "copyright 2011 Blender  
Foundation, <your name>" statement too. (This will still be BF  
copyright, but at least with a proper personal credit).
- I can make a BF letterhead pdf for you that supports and confirms  
commissioning this work.
</blockquote>

Ton Roosendaal: http://lists.blender.org/pipermail/bf-committers/2011-August/033278.html