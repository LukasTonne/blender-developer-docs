= Blender 2.92: User Interface =

== Outliner ==
* Properties editor sync. Clicking on the icon for outliner data will switch to the corresponding properties editor tab for properties editors that share a border. ([https://projects.blender.org/blender/blender/commit/0e47e57eb77d 0e47e57eb7])
* New filter to display only selectable objects. ([https://projects.blender.org/blender/blender/commit/fbf29086745f fbf2908674])
* The object state filter can now be inverted. For example, inverting "Selectable" will only display unselectable objects. ([https://projects.blender.org/blender/blender/commit/2afdb4ba875 2afdb4ba87])
* List library overrides in the Outliner. A filter option allows hiding them ([https://projects.blender.org/blender/blender/commit/aa64fd69e733 aa64fd69e7]).

== Properties Editor ==

* Add a new tab for collection properties ([https://projects.blender.org/blender/blender/commit/3e87d8a4315d 3e87d8a431]).

== 3D Viewport ==

* Use consistent name for select mirror across object modes ([https://projects.blender.org/blender/blender/commit/1c357a3c5fc1 1c357a3c5f]).
* Cycling object selection is now ordered by depth (nearest first) ([https://projects.blender.org/blender/blender/commit/3e5e204c81a2aa136ad645155494b7ab132db2bf 3e5e204c81]).

== General Changes ==

* Support tooltips for superimposed icons ([https://projects.blender.org/blender/blender/commit/2250b5cefee7 2250b5cefe]).
* Improvements to the alignment and placement of interface items ([https://projects.blender.org/blender/blender/commit/6bf043ac946f 6bf043ac94]) ([https://projects.blender.org/blender/blender/commit/b0a9a04f6201 b0a9a04f62]).
* Allow theming the alternate row color in the sequencer, similar to the file browser and the outliner ([https://projects.blender.org/blender/blender/commit/f7223d5f722ac4 f7223d5f72]).
* When dragging, constrain panels vertically in the property editor, preferences, and side-bars vertically ([https://projects.blender.org/blender/blender/commit/328aad8c98c931c3 328aad8c98]).
* When using Weight Paint Sample Weight Tool, using eyedropper cursor and display weight in header ([https://projects.blender.org/blender/blender/commit/64277e8f3a7e 64277e8f3a]) ([https://projects.blender.org/blender/blender/commit/e3d9241a0532 e3d9241a05]).
* New layout for the "About" dialog with full Blender logo  ([https://projects.blender.org/blender/blender/commit/79eeabafb39f 79eeabafb3]).
[[File:About Dialog.png|600px|thumb|center]]
* New look for the "Startup Script Execution" warning dialog  ([https://projects.blender.org/blender/blender/commit/2e5d9a73f7b9 2e5d9a73f7]).
[[File:Security-alert-dialog-box.png|600px|thumb|center]]
* Improved layout of node group socket lists ([https://projects.blender.org/blender/blender/commit/1d3b92bdeabc 1d3b92bdea]).
* New properties editor popover that contains settings for outliner to properties sync ([https://projects.blender.org/blender/blender/commit/ffacce5be41 ffacce5be4])