= Blender 2.92: Sculpting =

* Sculpt: Grab silhouette option [https://projects.blender.org/blender/blender/commit/383c20a6abd 383c20a6ab].

[[File:2020-10-22 19-14-59.mp4|600px|center|thumb]]

* Sculpt: Implement plane deformation falloff for Grab [https://projects.blender.org/blender/blender/commit/c53b6067fad c53b6067fa].
* Sculpt: Face Set Edit delete Geometry operation [https://projects.blender.org/blender/blender/commit/c15bd1c4e65 c15bd1c4e6].
* Sculpt/Paint: Add Paint Studio Light preset [https://projects.blender.org/blender/blender/commit/d6180dd2f7e d6180dd2f7].
* UI: Add Sculpt Session info to stats [https://projects.blender.org/blender/blender/commit/ea064133e53 ea064133e5].
* Sculpt: Allow inverting the Erase Displacement mesh filter [https://projects.blender.org/blender/blender/commit/1bc75dfa4a9 1bc75dfa4a].
* Sculpt: Elastic deform type for Snake Hook [https://projects.blender.org/blender/blender/commit/d870a60dd92 d870a60dd9].
* Sculpt: Multires Displacement Smear [https://projects.blender.org/blender/blender/commit/d23894d3ef3 d23894d3ef].
* Sculpt: Fair Face Sets operation for Face Set Edit [https://projects.blender.org/blender/blender/commit/c9b4d753362 c9b4d75336].