== Library Overrides ==
* Added limited support for simulation caches on override data-blocks (caches need to be written on disk for now, [https://projects.blender.org/blender/blender/commit/59910f72  rB59910f72], [https://projects.blender.org/blender/blender/commit/7e210e68  rB7e210e68], [https://projects.blender.org/blender/blender/commit/50ccf346  rB50ccf346]).
* Added initial support for NLA on override data-blocks (one can add new tracks to existing NLA system, and edit some properties of the existing tracks and strips ([https://projects.blender.org/blender/blender/commit/c0bd240ad0a1  rBc0bd240a]).
* Some heavy data from override IDs do not get written to disk in the local .blend file anymore (mainly mesh geometry, shapekeys data, and embedded files currently, see [http://developer.blender.org/D9810 D9810], [https://projects.blender.org/blender/blender/commit/f5a019ed43ab  rBf5a019ed]).

== glTF 2.0 ==

=== Importer ===
* Importer can now open Draco encoded files ([https://projects.blender.org/blender/blender-addons/commit/eb29a12da48e  rBAeb29a12])
* Implement occlusion strength ([https://projects.blender.org/blender/blender-addons/commit/d5c0d4b77c15  rBAd5c0d4b])
* Show specific error when buffer resource is missing ([https://projects.blender.org/blender/blender-addons/commit/285deaf22c27  rBA285deaf])
* Removed future deprecated encoding arg to json.loads ([https://projects.blender.org/blender/blender-addons/commit/48ec56bde5fc  rBA48ec56b])
* Create placeholder images for files that can't be loaded ([https://projects.blender.org/blender/blender-addons/commit/59f60554f18c  rBA59f6055])
* Make NLA track order match glTF animation order ([https://projects.blender.org/blender/blender-addons/commit/d7f8d9a4f919  rBAd7f8d9a])
* Use Separate R node for clearcoat textures ([https://projects.blender.org/blender/blender-addons/commit/ebdf1861dc56  rBAebdf186])

=== Exporter ===
* Fix export when texture is used only for alpha ([https://projects.blender.org/blender/blender-addons/commit/5f2cb885abb9  rBA5f2cb88])
* Take care of active output node ([https://projects.blender.org/blender/blender-addons/commit/5088c8d9d735  rBA5088c8d])
* Implement occlusion strength ([https://projects.blender.org/blender/blender-addons/commit/d5c0d4b77c15  rBAd5c0d4b])
* Avoid crash when background exporting (without UI) ([https://projects.blender.org/blender/blender-addons/commit/c5a84e2e1ad0  rBAc5a84e2])
* Fix texture filtering ([https://projects.blender.org/blender/blender-addons/commit/9ff0d983ee49  rBA9ff0d98])
* Roundtrip all texture wrap modes ([https://projects.blender.org/blender/blender-addons/commit/70efc485eca8  rBA70efc48])
* Various Draco encoder fixes ([https://projects.blender.org/blender/blender-addons/commit/eb29a12da48e  rBAeb29a12])
* fix collection offset when rotated or scaled ([https://projects.blender.org/blender/blender-addons/commit/3e7209f9f289  rBA3e7209f])
* More fixes for changing the filename when format changed ([https://projects.blender.org/blender/blender-addons/commit/3ce41afdfa56  rBA3ce41af])