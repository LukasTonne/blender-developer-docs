= Geometry Nodes =

== Node-Based Tools ==

[https://docs.blender.org/manual/en/4.0/modeling/geometry_nodes/tools.html Node tools] are an accessible way to expand Blender and customize tools without requiring Python.

[[File:Edge to mesh node tools demo.mp4|center|thumb|500px|Edge to mesh node tools demo by Tim Greenberg (passivestar).]]

* Geometry node node groups can now be used as operators from the 3D view menus.
* Specific nodes are available for controlling tool-specific data:
** Edit mode selection is accessible with the '''Selection''' and controllable with the '''Set Selection''' node.
** Sculpt face sets are accessible as well with the '''Face Set''' and '''Set Face Set''' nodes.
** The '''3D Cursor''' node gives access to the 3D cursor location and rotation.
* Documentation is available in the [https://docs.blender.org/manual/en/4.0/modeling/geometry_nodes/tools.html user manual].
* See the [https://projects.blender.org/blender/blender/issues/101778 overview task] for more information.
* Read the [https://code.blender.org/2023/10/node-tools/ announcement blog post].
* Watch the [https://www.youtube.com/watch?v=rU_j1Jw3ev4 live demo video].

== General ==
* The new '''Repeat''' zone allows repeating nodes a dynamic number of times. [https://docs.blender.org/manual/en/4.0/modeling/geometry_nodes/utilities/repeat_zone.html See Manual]. ([https://projects.blender.org/blender/blender/commit/3d73b71a97 3d73b71a97], [https://projects.blender.org/blender/blender/commit/c8cc169d6fee3 c8cc169d6f])

[[File:Modeling geometry-nodes repeat zone.png|thumb|800px|center]]

* The mesh sharp edge status is accessible in builtin nodes. ([https://projects.blender.org/blender/blender/commit/4e97def8a32147b245759f04ceceeefd937b43f5 4e97def8a3])

[[File:Sharp Edge Nodes.png|500px|thumb|center|A new node and a modification to the "Set Shade Smooth" node to control the sharpness of mesh edges.]]

* The ''Mesh to Volume'' node now generates a proper fog volume rather than a converted SDF. ([https://projects.blender.org/blender/blender/commit/700d168a5c7 700d168a5c])
** The "Exterior Band Width" and "Fill Interior" options have been removed.
* The '''Points to Curves''' node groups point cloud points into curves and sorts each curve's points based on a weight. ([https://projects.blender.org/blender/blender/commit/48b08199d5dde4efb7c4d08342017a6c77df056c 48b08199d5])
* A new debug utility helps to check procedural systems for bad index dependence. ([https://projects.blender.org/blender/blender/commit/cc7da09c1b820b91b8cfc7639893284e2bfc7ac2 cc7da09c1b])
* Rotation sockets and eight new rotation nodes are available for simpler processing. [https://docs.blender.org/manual/en/4.0/modeling/geometry_nodes/utilities/rotation/index.html See Manual]. ([https://projects.blender.org/blender/blender/commit/34e4bedcd8a79ddff1a9e7d77ea3f5a99071b61f 34e4bedcd8]).
** '''Axis Angle to Rotation'''
** '''Rotation to Axis Angle'''
** '''Quaternion to Rotation'''
** '''Rotation to Quaternion'''
** '''Euler to Rotation'''
** '''Rotation to Euler'''
** '''Rotate Vector'''
** '''Invert Rotation'''
* Simulation zones can now be baked individually. ([https://projects.blender.org/blender/blender/commit/ad169ba67a ad169ba67a])
* The ''Simulation Output'' node has a new Skip input. ([https://projects.blender.org/blender/blender/commit/dd515ebc1df dd515ebc1d])

== Modifiers ==

* The "Add Modifier" menu has been changed to a standard menu and is extended with custom modifiers from geometry node group assets. ([https://projects.blender.org/blender/blender/commit/6da4b87661f0fbbe7fc567b3cc3b6eb320bf87a2 6da4b87661])
** When geometry node groups are marked as assets and the "Is Modifier" property is enabled, the asset catalog path is used to dynamically create the modifier menu.
** Assets unassigned to any node group are displayed in a separate "Unassigned" category. ([https://projects.blender.org/blender/blender/commit/d2d4de8c710c4e8e648a86f904e6436133664836 d2d4de8c71])
* Two new options help to clean up the modifier interface
** The node group selector can be disabled, which happens by default for node group asset modifiers. ([https://projects.blender.org/blender/blender/commit/0783debda88779c6ee56281d08ac0b2d3b3d4dd9 0783debda8])
** Inputs can be forced to be single values to remove the attribute toggle. ([https://projects.blender.org/blender/blender/commit/6875925efa2acdd25eec5165915b2c79533746d8 6875925efa])
*** This also removes the attribute toggle in the node tool redo panel.

<gallery mode="packed" widths=250px heights=100px perrow=4>
File:Modifier Interface Cleanup Before.png|The node group toggle and the attribute toggle are visible in the modifier
File:Modifier Interface Cleanup After.png|No attribute toggle or node group selector
</gallery>

== Performance ==
* The ''Curve to Mesh'' node is significantly faster when the profile input is a single point.
** When the input point is at the origin, tangent, normal, and radii calculations are skipped, giving a 2x improvement. ([https://projects.blender.org/blender/blender/commit/6e9f54cbdadb529a30b38cc5e1c87d24efbcda88 6e9f54cbda])
** Handling with many point-domain attributes is improved, with an observed 4x improvement. ([https://projects.blender.org/blender/blender/commit/48fad9cd0c9693e4dbb31d94c349260e486d45d9 48fad9cd0c])
** Further improvements avoiding copying attributes entirely are possible when all curves are poly curves. ([https://projects.blender.org/blender/blender/commit/bef20cd3f1e77105b67fb90f0cd4d576ed851545 bef20cd3f1])