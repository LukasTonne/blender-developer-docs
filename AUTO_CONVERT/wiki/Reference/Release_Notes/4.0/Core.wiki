= Core =

== Blend File Compatibility ==
* The [[Process/Compatibility_Handling|new blendfile compatibility policy]] has been implemented. ([https://projects.blender.org/blender/blender/issues/109151 #109151], [https://projects.blender.org/blender/blender/pulls/110109 PR #110109])
* Very early 2.80 development 'scene collection' compatibility code as been removed. This does not affect any file produced by a released version of Blender. ([https://projects.blender.org/blender/blender/issues/110918 #110918], [https://projects.blender.org/blender/blender/pulls/110926 PR #110926], [https://projects.blender.org/blender/blender/commit/23835a393c 23835a393c])

== Data-Blocks ==

* Custom Properties can now be defined from the UI to store references to other data-blocks ([https://projects.blender.org/blender/blender/commit/b3c7f3c8a9 b3c7f3c8a9]). Previously, this was only doable through Python API.
 
== Library Overrides ==

* Many fixes and improvements were made, as part of the [https://www.youtube.com/watch?v=u9lj-c29dxI&pp=ygUPd2luZyBpdCBibGVuZGVy Blender Studio's "WING IT!"] production. ([https://projects.blender.org/blender/blender/pulls/109704 PR #109704])
** Parenting-related Object properties (the parent inverse matrix, parent type, parent sub-target) will now be reset to the linked reference data when parent pointer itself is modified in library file and updated during resync process ([https://projects.blender.org/blender/blender/commit/9ed5177055 9ed5177055], [https://projects.blender.org/blender/blender/commit/4842424220 4842424220]).
** Cleanup-code pruning unused liboverride operations during RNA diffing would not always work properly, leading to accumulated invalid operations that could, under certain circumstances, end up severely breaking especially collections/objects hierarchies of liboverrides ([https://projects.blender.org/blender/blender/commit/784d09a87c 784d09a87c]).
** Resync Enforce tool would not always work properly with collections of IDs (affecting especially Collections/Objects relationships) ([https://projects.blender.org/blender/blender/commit/2dfbd653a0 2dfbd653a0], [https://projects.blender.org/blender/blender/commit/1c0ffa1e1816a5 1c0ffa1e18]).
** Code trying to fix invalid liboverride hierarchies was over-enthusiastic, which could pull data out of their valid original hierarchy into some more local, undesired one when production files start to become messy ([https://projects.blender.org/blender/blender/commit/bf93fb0f46 bf93fb0f46]).
** Handling of lookup of items in RNA collections has been improved, to alleviate issues when there are name collisions between items ([https://projects.blender.org/blender/blender/commit/a05419f18b a05419f18b]).
** As a second approach to work around issue described above, changes were made to liboverride IDs naming, such that when created, they either get the exact same name as their linked reference, or they get a name that does not collide with any other linked override reference ([https://projects.blender.org/blender/blender/commit/b9becc47de b9becc47de], [https://projects.blender.org/blender/blender/commit/e11da03e7a e11da03e7a]).

== Breaking Changes ==

* The Depth pass support was removed from OpenEXR (the non-multilayer case) and Iris ([https://projects.blender.org/blender/blender/commit/e1b60fdb913ebe60a57e2459f0d1fbc1f921e643 e1b60fdb91])
* Unused linked data is no longer kept when saving and re-opening a blendfile. This was already partially the case in previous releases.
** The 'Fake User' setting is now fully irrelevant for linked data, and systematically cleared on load or link.
** When an unused (as in, having zero users) linked data-block needs to be kept in a blendfile, users are expected to reference them through a Custom Property (e.g. from a Scene, Collection or Object data-block).
** See [https://projects.blender.org/blender/blender/issues/106321 #106321] and the related commits [https://projects.blender.org/blender/blender/commit/b3c7f3c8a9 b3c7f3c8a9] and [https://projects.blender.org/blender/blender/commit/f052b18a65 f052b18a65].
* Keep some animation data from library overrides when no animation data existed originally in the linked reference, but it got added later on in the library file. Previously, any change related to animation done in the liboverride data would have been lost ([https://projects.blender.org/blender/blender/issues/110067 #110067], [https://projects.blender.org/blender/blender/pulls/110900 PR #110900], [https://projects.blender.org/blender/blender/commit/f188d6709f f188d6709f]).