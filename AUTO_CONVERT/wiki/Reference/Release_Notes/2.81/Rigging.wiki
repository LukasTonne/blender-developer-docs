= Blender 2.81: Animation & Rigging =

== Animation Curves ==

* The default automatic handle [https://docs.blender.org/manual/en/dev/editors/graph_editor/fcurves/properties.html#graph-editor-auto-handle-smoothing smoothing mode] for new F-Curves can now be changed via User Preferences -> Animation. ([https://projects.blender.org/blender/blender/commit/aabd8701 aabd8701])

== Bones ==

* The Inherit Scale checkbox is replaced with a dropdown providing [https://docs.blender.org/manual/en/dev/animation/armatures/bones/properties/relations.html#transformations more options], e.g. Inherit Average Scale. ([https://projects.blender.org/blender/blender/commit/fcf2a712 fcf2a712])

== Drivers ==

* New [https://docs.blender.org/manual/en/dev/animation/drivers/usage.html#copy-as-new-driver Copy As New Driver] context menu option to streamline creating drivers that copy the value of a different property. ([https://projects.blender.org/blender/blender/commit/47335b4e 47335b4e])
* Support overriding the Euler order and access Quaternion values in rotational Transform Channel driver variables. ([https://projects.blender.org/blender/blender/commit/82ef1edc 82ef1edc], [https://projects.blender.org/blender/blender/commit/88370639 88370639])
* [https://docs.blender.org/manual/en/dev/animation/drivers/drivers_panel.html#rotation-channel-modes Swing + Twist] rotation decomposition to Transform Channel driver variables. ([https://projects.blender.org/blender/blender/commit/e91ea20e e91ea20e])

== Constraints ==

=== Copy Scale ===

* [https://docs.blender.org/manual/en/dev/animation/constraints/transform/copy_scale.html#options New option] to force the copied scale to be uniform. ([https://projects.blender.org/blender/blender/commit/49729785 49729785])

=== Copy Rotation ===

* Option to explicitly specify the Euler order used during copy. ([https://projects.blender.org/blender/blender/commit/18d4ad5a 18d4ad5a])
* Replaced the Offset checkbox with a [https://docs.blender.org/manual/en/dev/animation/constraints/transform/copy_rotation.html#options Mix dropdown] providing better combining options. ([https://projects.blender.org/blender/blender/commit/f4056e9e f4056e9e])

=== Copy Transforms ===

* Added a new [https://docs.blender.org/manual/en/dev/animation/constraints/transform/copy_transforms.html#options Mix dropdown] option that allows combining the original and copied transforms. ([https://projects.blender.org/blender/blender/commit/f8362836 f8362836])

=== Transformation ===

* Options to explicitly specify the input and output Euler order. ([https://projects.blender.org/blender/blender/commit/18d4ad5a 18d4ad5a])
* Swing + Twist rotation decomposition as a choice for rotation [https://docs.blender.org/manual/en/dev/animation/constraints/transform/transformation.html#source input mode]. ([https://projects.blender.org/blender/blender/commit/e91ea20e e91ea20e])
* [https://docs.blender.org/manual/en/dev/animation/constraints/transform/transformation.html#destination Mix dropdown] option that allows changing how the transformation is combined with the original. ([https://projects.blender.org/blender/blender/commit/e858d21a e858d21a])

== Custom Properties ==

* [https://docs.blender.org/manual/en/dev/files/data_blocks.html#custom-properties Custom property] UI now supports short arrays, useful for storing vectors or colors as a single property. ([https://projects.blender.org/blender/blender/commit/aef08fda aef08fda])
* Array custom properties can be set to appear as a standard color picker button in the UI. ([https://projects.blender.org/blender/blender/commit/55c38f47 55c38f47])