= Blender 2.81: Grease Pencil =

== User Interface ==

* Edit Mode Menu reorganization: The old single stroke menu was divided into Grease Pencil, Stroke and Point menu. ([https://projects.blender.org/blender/blender/commit/810caad80e6f 810caad80e])
* Edit Mode Context Menu reorganization: The old single context menu was divided into Stroke and Point menu. ([https://projects.blender.org/blender/blender/commit/8c7cbad54274 8c7cbad542])
* Draw Mode Menu updates ([https://projects.blender.org/blender/blender/commit/0067b3b09b1c 0067b3b09b])
* Mask Selection in Sculpt Mode now support modes for selection (point, stroke, In between) Separately from Edit Mode selection modes.
* Stroke placement now use smaller numbers to determine the offset
* Object data updates: Opacity and Blend controls have been changed for consistency, Mask control is accesible now only at layer list and ''Show only On Keyframed toggle'' was moved to Display Layer section. ([https://projects.blender.org/blender/blender/commit/d795dd1fa780 d795dd1fa7])
* Paste to active layer by default and change names. ([https://projects.blender.org/blender/blender/commit/51d9f56f874d 51d9f56f87])
* When set Stroke Select Mode in Edit, the edit points are not displayed and the current selection is extended to the full stroke for any selected stroke. ([https://projects.blender.org/blender/blender/commit/6b33bd1067dc 6b33bd1067])
* Overlays: a new property to include Grease Pencil objects to the ''Fade 3D Object'' control was added. Name was changed to ''Fade Object'' to reflect the change. ([https://projects.blender.org/blender/blender/commit/a5a003ed5589 a5a003ed55])

[[File:Fade Gpencil object.png|600px|thumb|center|The non-active Grease Pencil objects and meshes faded]]

* Overlays: ''Fade layers'' using background color and available in all modes. ([https://projects.blender.org/blender/blender/commit/74dcfaf1722f 74dcfaf172])
* Menu items order in ''Arrange Menu''  changed to make more consistent with the distance. ([https://projects.blender.org/blender/blender/commit/e164afe9b5f9 e164afe9b5])
* New Simplify Layer Tinting to disable all tints in one step.
* New postprocesing Simplify option for brushes.

== Operators ==

* New ''Merge by Distance'' operator (Clean Up Menu). ([https://projects.blender.org/blender/blender/commit/b9d0f33530f0 b9d0f33530])
* New ''Simplify Sample'' operator.
* Improved algorith for the ''Smooth Operator'' when using Thickness and Strength. ([https://projects.blender.org/blender/blender/commit/0e1d4dec7a7d 0e1d4dec7a])
* New option to convert Curves to Grease Pencil strokes. ([https://projects.blender.org/blender/blender/commit/505340202e96 505340202e])

[[File:Convert Curves to Grease Pencil operator.png|600px|thumb|center|Convert Curves to Grease Pencil operator]]

* New ''Set as Active Material'' to establish the active object material based on the selected stroke material. ([https://projects.blender.org/blender/blender/commit/d0462dca9071 d0462dca90])

== Tools ==

* Improvement in ''Guides'': Better snapping, new ISO grid option for lines specified by Angle, radial snapping mode uses Angle as an offset ([https://projects.blender.org/blender/blender/commit/a94bf0e1349b a94bf0e134])

[[File:ISO guides.png|600px|thumb|center|Isometric Guides sample (45°)]]

* Improvements in Strokes ending. ([https://developer.blender.org/rBb393b135245e b393b135245e])

[[File:Strokes ending.png|600px|thumb|center|Better strokes ending when drawing lines in a fast way]]

* Improvements when move mouse/pen very fast - Smart Smooth. ([https://developer.blender.org/rB12688a6636da 12688a6636da])
* New Brushes and presets. ([https://projects.blender.org/blender/blender/commit/3c7707b49fc6 3c7707b49f])

[[File:New_Brushes.png|600px|thumb|center|New Brushes and presets]]

== Modifiers ==
* Move Modifiers evaluation from the Draw Manager to the Depsgraph. ([https://projects.blender.org/blender/blender/commit/ee4ec69b2804 ee4ec69b28] [https://projects.blender.org/blender/blender/commit/091e7979d314 091e7979d3] [https://projects.blender.org/blender/blender/commit/a67f218b5417 a67f218b54] [https://projects.blender.org/blender/blender/commit/2dd1e6b376bf 2dd1e6b376])
* Improved ''Noise Modifier'' with a new seed value.
* Grease Pencil Modifiers have a new influence filter by a single material ([https://projects.blender.org/blender/blender/commit/3d8f1586973b 3d8f158697])
* ''Simplify Modifier'' has two new modes: Sample and Merge
* ''Opacity Modifier'' has a new Strength mode ([https://projects.blender.org/blender/blender/commit/591db72ee2ee 591db72ee2])

== Materials ==

* New ''Self Overlap'' parameter in materials to disabled the stencil effect on the strokes.

[[File:Self Overlap.png|600px|thumb|center|Self Overlapped stroke and Stencil stroke]]

* New ''Default Material'' for strokes when material slots are empty ([https://projects.blender.org/blender/blender/commit/d5d3dc418912 d5d3dc4189])