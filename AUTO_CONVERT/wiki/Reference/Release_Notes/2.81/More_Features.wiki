= Blender 2.81: More Features =

== Alembic ==

* Face-varying normals are now imported from Alembic. ([https://projects.blender.org/blender/blender/commit/e9c149d911c e9c149d911]).
* Importing is now possible with relative paths, and respects the 'Relative Paths' user preference. ([https://projects.blender.org/blender/blender/commit/34143e45104b 34143e4510]).
* The Auto Smooth flag can be used to toggle flat/smooth shading on an imported mesh. This only works when the Alembic mesh doesn't contain face normals (if it does contain normals, those are used as-is). ([https://projects.blender.org/blender/blender/commit/dffe702d782 dffe702d78]).

== Audio & Video Output ==

* Opus audio codec support. ([https://projects.blender.org/blender/blender/commit/2ddfd51810e0 2ddfd51810])
* WebM video container support. ([https://projects.blender.org/blender/blender/commit/ca0b5529d09c ca0b5529d0])
* Alpha channel support for WebM/VP9 video. ([https://projects.blender.org/blender/blender/commit/43b7512a59c2 43b7512a59])
* RGB mode (contrary to RGBA) support for various video formats, like Quicktime RLE/Animation. Previously these always included an alpha channel. ([https://projects.blender.org/blender/blender/commit/08a63215018 08a6321501]).

== Library Management ==

* Only directly linked objects are now added to the scene when appending. Indirectly linked objects will be used by the directly linked objects but not visible in the scene. ([https://projects.blender.org/blender/blender/commit/37b4384b59c0968  rB37b4384b]).

== Sequencer ==

* Prefetching support, to automatically fill the cache with frames after the current frame in the background. This can be enabled in the sequencer Preview, in the View Settings in the sidebar ([https://projects.blender.org/blender/blender/commit/ab3a9dc1ed28  rBab3a9dc1]).
* New operator to add and remove fades for all strips(volume or opacity property) ([https://projects.blender.org/blender/blender/commit/2ec025d7be3c  rB2ec025d7])