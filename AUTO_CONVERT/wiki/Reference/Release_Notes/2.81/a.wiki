== Blender 2.81: Bug Fixes ==

=== Blender ===

* Fix [http://developer.blender.org/T71147 #71147]: Eevee stops rendering after selection attempt ([https://projects.blender.org/blender/blender/commit/4e42a98edd8  rB4e42a98e])
* Fix [http://developer.blender.org/T71213 #71213]: Mask or Mirror before Armature breaks weight paint ([https://projects.blender.org/blender/blender/commit/f1ac64921b4  rBf1ac6492] )
* Fix [http://developer.blender.org/T71612 #71612]: Viewport rotate doesn't work ([https://projects.blender.org/blender/blender/commit/aadbb794cd6e  rBaadbb794])
* Fix [http://developer.blender.org/T71741 #71741]: Crash showing the object relations menu ([https://projects.blender.org/blender/blender/commit/8cb55f8d1672  rB8cb55f8d])
* Fix [http://developer.blender.org/T71558 #71558]: Hair particles: Brush effect not occluded by emitter geometry ([https://projects.blender.org/blender/blender/commit/a8d29ad6e06  rBa8d29ad6])
* Fix segfault when polling `MESH_OT_paint_mask_extract` ([https://projects.blender.org/blender/blender/commit/73ce35d3325  rB73ce35d3])
* Fix [http://developer.blender.org/T71864 #71864]: Broken 'Select' > 'More' in face mode in UV Editor ([https://projects.blender.org/blender/blender/commit/bdfcee347eb  rBbdfcee34])
* Fix [http://developer.blender.org/T69332 #69332]: 'Reset to Default Value' on a custom string property crashes ([https://projects.blender.org/blender/blender/commit/60e817693ce  rB60e81769])
* Fix [http://developer.blender.org/T72071 #72071]: Crash on snap to edge ([https://projects.blender.org/blender/blender/commit/4a440ecb99d  rB4a440ecb])
* Fix crash exiting edit-mode with an active basis shape key ([https://projects.blender.org/blender/blender/commit/34902f20089 34902f2008])

=== Addons ===
* Fix [http://developer.blender.org/T71774 #71774]: SVG import error on specific files ([https://projects.blender.org/blender/blender-addons/commit/ee818b2a  rBAee818b2])
* Fix [http://developer.blender.org/T71678 #71678]: Rigify crash if the Advanced mode is set to New. ([https://projects.blender.org/blender/blender-addons/commit/ed5b81f5  rBAed5b81f])
* Fix [http://developer.blender.org/T72145 #72145]: STL crash exporting object without data ([https://projects.blender.org/blender/blender-addons/commit/2f425cc1  rBA2f425cc])