= Blender 2.90 Release Notes =

Blender 2.90 was released on August 31, 2020.

Check out the final [https://www.blender.org/download/releases/2-90/ release notes on blender.org]. List of [[Reference/Release Notes/2.90/Corrective_Releases|corrective releases]].

== [[Reference/Release Notes/2.90/User_Interface|User Interface]] ==

== [[Reference/Release Notes/2.90/Modeling|Modeling]] ==

== [[Reference/Release Notes/2.90/Sculpt|Sculpt]] ==

== [[Reference/Release Notes/2.90/Grease_Pencil|Grease Pencil]] ==

== [[Reference/Release Notes/2.90/Cycles|Cycles]] ==

== [[Reference/Release Notes/2.90/EEVEE|EEVEE]] ==

== [[Reference/Release Notes/2.90/IO|Import & Export]] ==

== [[Reference/Release Notes/2.90/Python_API|Python API]] ==

== [[Reference/Release Notes/2.90/Physics|Physics]] ==

== [[Reference/Release Notes/2.90/Animation-Rigging|Animation & Rigging]] ==

== [[Reference/Release Notes/2.90/Virtual_Reality|Virtual Reality]] ==

== [[Reference/Release Notes/2.90/More_Features|More Features]] ==

== [[Reference/Release Notes/2.90/Add-ons|Add-ons]] ==

== Compatibility ==

* The minimum required macOS version was changed from 10.12 Sierra to 10.13 High Sierra. Apple extended support for Sierra ended in October 2019.

== [[Reference/Release Notes/2.90/Corrective_Releases|Corrective Releases]] ==