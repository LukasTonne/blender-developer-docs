= Blender 3.0: EEVEE = 

== Attribute Node ==
Custom mesh attributes (generated for example by Geometry Nodes) are now accessible via the Attribute node. ([https://projects.blender.org/blender/blender/commit/03013d19d16704672f9db93bc62547651b6a5cb8 03013d19d1])

== Wavelength Node ==
Support for wavelength node has been added to EEVEE, earlier it was Cycles only. It is now similar to Blackbody node, which uses a sampled color map internally. [https://projects.blender.org/blender/blender/commit/a43c7538b802 a43c7538b8]

== Performance ==

Performance when editing huge mesh is improved. Changes in many areas are made resulting in an improvement of 2-3 times depending on the executed operation.

For a complete list of changes see [http://developer.blender.org/T88550 #88550].