= Animation & Rigging =

== Redo Panel ==

* The NLA, Dopesheet, and Timeline editors now have a Redo panel ([https://projects.blender.org/blender/blender/commit/1f828a5a064f  rB1f828a5a], [https://projects.blender.org/blender/blender/commit/3132d2751e61  rB3132d275]). Some properties shown in these redo panels may be superfluous (like having a Z-axis when moving keys around); this is a known limitation.

== NLA ==

* Draw track background based on strip's extrapolation type ([https://projects.blender.org/blender/blender/commit/2310daed3a55  rB2310daed]).
[[File:blender-3.4-nla-extrapolation-drawing.png|340px|thumb|center]]
* Adding an action clip now fails immediately if no NLA track is selected ([https://projects.blender.org/blender/blender/commit/ddfce277e0cb  rBddfce277]). Previously this only failed after you selected a specific action to add.
* Removed the "Edited Action" tab for selected Action strips in the NLA editor ([https://projects.blender.org/blender/blender/commit/b6ebd5591c7f  rBb6ebd559]). It is still available in the Action editor, where it is actually suitable/usable. Having it in the NLA got in the way of the actual NLA strip properties. These are now available immediately by default.
* Pushing down an action to a new NLA track now automatically names that track after the Action ([https://projects.blender.org/blender/blender/commit/78fe6d7ab195  rB78fe6d7a]).

== Driver Mute ==

The driver editor and the "Edit Driver" popover now have a checkbox that can mute the driver ([https://projects.blender.org/blender/blender/commit/c592bff04745  rBc592bff0]). This is the same functionality as the checkbox in the driver editor's channel list, but then exposed in a different place in the UI. This is for convenience, such that a driver can now be muted by right-clicking on the driven property, choosing "Edit Driver", then muting it there. The same checkbox was added to the regular driver editor's header for consistency.

<gallery mode="packed" widths=250px heights=280px>
File:blender-3.4-driver-mute.png|Driver Editor
File:blender-3.4-driver-mute-popover.png|Edit Driver pop-over
</gallery>