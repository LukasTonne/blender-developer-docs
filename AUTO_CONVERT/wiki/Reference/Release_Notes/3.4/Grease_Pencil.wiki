= Grease Pencil =
== Operators ==
* Now SVG import allows to import several files in one operation. Also, the name of the .SVG is used as Blender name. ([https://projects.blender.org/blender/blender/commit/670ced97589d 670ced9758])
* New `Outline` operator to convert strokes to perimeter. ([https://projects.blender.org/blender/blender/commit/aa7b2f1dd9f7 aa7b2f1dd9])
* New `Outline` option at brush level to convert to outline the last stroke drawn. ([https://projects.blender.org/blender/blender/commit/613b6ad9e55f 613b6ad9e5])
* New `Set Start Point`operator to define the start point for cyclic strokes. ([https://projects.blender.org/blender/blender/commit/98c4e1e590cb 98c4e1e590])
* New `Offset` parameter in reproject operator for surface mode. ([https://projects.blender.org/blender/blender/commit/5c33704ffa2f 5c33704ffa])

== Tools ==
* Fill tool has been improved adding a new algorithm to close gaps. ([https://projects.blender.org/blender/blender/commit/468f2ccc0ecb 468f2ccc0e], [https://projects.blender.org/blender/blender/commit/dd0ef7488705 dd0ef74887], [https://projects.blender.org/blender/blender/commit/742268c50bd4 742268c50b], [https://projects.blender.org/blender/blender/commit/bdbf24772a0d bdbf24772a], [https://projects.blender.org/blender/blender/commit/392855ce5022 392855ce50], [https://projects.blender.org/blender/blender/commit/fe19de5fccac fe19de5fcc])

The new method determines when to close the strokes based on how close they are using a circle radius. This new method is better suited than the old one (now renamed Extend) when the extended strokes never cross

If the gap between strokes is greater than the actual radius, the system shows circles. To change the size of the circle use `Wheel Mouse` or `PageUp` / `PageDown`. Also it's possible to use `MMB` of the mouse or pen.

[[File:Close_Open.png|400px]] 

As soon the radius determine that the gap is closed, the help circles are hidden. This allows to focus in pending gaps, keeping only circles in pending ones.

[[File:Close_Contact.png|400px]]

The Advanced panel was rearranged and now has a new section for `Gap Closure` with all the new options.

[[File:Panel_Fill_Tool_new.jpg|486px]]

Note: The old gap closure method using straight lines has been renamed as `Extend` and the `Leak Size` has been removed because new algorithms make this parameter obsolete.

* Fill tool extend lines now are clipped when there is a collision. ([https://projects.blender.org/blender/blender/commit/5c13c7cd30d1 5c13c7cd30])
* New option to check if extension collide with normal strokes (not extensions). If enabled, if the extension collide, the color will be blue. Only blue extension will be used to limit fill area.
* Use `S` key to toggle extend method from Extend->Radius->Extend.
* Use `D` key to toggle stroke collision check.

== Modifiers ==

* New Outline modifier to generate perimeter stroke from camera view. ([https://projects.blender.org/blender/blender/commit/5f7259a0013b 5f7259a001])  
* LineArt: New "Forced Intersection" option that allows objects to always produce intersections even against objects that are set as "No Intersection". This allows more flexible configuration of more complex scenes. ([https://projects.blender.org/blender/blender/commit/0bdb5239c1a5 0bdb5239c1])
* New `Chain` mode in Time Offset modifier that allows run one after another different Offset types. ([https://projects.blender.org/blender/blender/commit/b0d70a9c8040 b0d70a9c80])

[[File:Time Offset.png|346px]]

== Python API ==
* New Trace Frame parameter in Trace operator. ([https://projects.blender.org/blender/blender/commit/903709c4ebd7 903709c4eb])
The default trace can only trace an image or a sequence, but it was not possible to trace only selected frames in a sequence. 
This new parameter allows to define what frame trace. If the value is 0, the trace is done as before. The parameter is not exposed in the UI because this is only logic if it is managed by a python API.