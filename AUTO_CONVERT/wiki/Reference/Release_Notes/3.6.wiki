= Blender 3.6 LTS Release Notes =

Blender 3.6 was released on June 27, 2023.

Check out out the final [https://www.blender.org/download/releases/3-6/ release notes on blender.org].

This release includes long-term support, see the [https://www.blender.org/download/lts/ LTS page] for a list of bugfixes included in the latest version.

== [[Reference/Release Notes/3.6/Animation_Rigging|Animation & Rigging]] ==

== [[Reference/Release Notes/3.6/Core|Core]] ==

== [[Reference/Release Notes/3.6/EEVEE|EEVEE & Viewport]] ==

== [[Reference/Release Notes/3.6/Grease_Pencil|Grease Pencil]] ==

== [[Reference/Release Notes/3.6/Modeling|Modeling & UV]] ==

== [[Reference/Release Notes/3.6/Nodes_Physics|Nodes & Physics]] ==

== [[Reference/Release Notes/3.6/Pipeline_Assets_IO|Pipeline, Assets & I/O]] ==

== [[Reference/Release Notes/3.6/Python_API|Python API & Text Editor]] ==

== [[Reference/Release Notes/3.6/Cycles|Render & Cycles]] ==

== [[Reference/Release Notes/3.6/Sculpt|Sculpt, Paint, Texture]] ==

== [[Reference/Release Notes/3.6/User_Interface|User Interface]] ==

== [[Reference/Release Notes/3.6/Add-ons|Add-ons]] ==

== [[Reference/Release Notes/3.6/Asset_Bundles|Asset Bundles]] ==

== Compatibility ==