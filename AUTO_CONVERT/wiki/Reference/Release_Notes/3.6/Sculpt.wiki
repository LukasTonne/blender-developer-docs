= Sculpt, Paint, Texture =

== Features ==

* Add support for "Adjust Last Operation" panel to mesh & color filters ([https://projects.blender.org/blender/blender/commit/c352eeb2134 c352eeb213]) ([https://projects.blender.org/blender/blender/commit/b4ee9366278a8159af5e2b97bfc18ccc29354aa3 b4ee936627])

[[File:Filter repeat last.png|400px|frameless]]

* Transform, Trim, Project, Fairing and Filter operations are now also available in the header menu ([https://projects.blender.org/blender/blender/commit/da65b21e2e0 da65b21e2e]) 
This makes it possible to easily assign these operation a shortcut or add them to the Quick Favorites menu for faster access.

Repeated use is possible with Repeat Last (`Shift+R`).

[[File:Sculpt menu update.png|frameless]]

* Added trim orientation to Box Trim UI. Previously only available in the Lasso Trim tool ([https://projects.blender.org/blender/blender/commit/a843a9c9bb8a25f123752fcd6ca68f4694974a44 a843a9c9bb])