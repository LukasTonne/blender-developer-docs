= Animation & Rigging =

== General ==

* Slider Operators are now available with hotkeys. ALT+S for smoothing operators and ALT+D for blending operators. [https://projects.blender.org/blender/blender/pulls/107866 PR #107866] Animation: change Slider Popup menu hotkeys
* The iTaSC IK solver now supports keeping root bones (i.e. without parent) positioned where they are, instead of always moving them to the armature's origin. For backward compatibility the old behavior is retained; new armatures will get the new behavior. This can be controlled via the new "Translate Roots" checkbox. ([https://projects.blender.org/blender/blender/commit/51c2a0f81606d2920146bf3fd3e753148d7c7aca 51c2a0f816])
* `Ctrl-LMB` shortcut for channel renaming is removed. It is now used for Extend channel selection ([https://projects.blender.org/blender/blender/commit/6c4da68ad2320608798e6418b5bdd5222053686d 6c4da68ad2]).

=== Extend Paint Mask Selection ===
Add the option to grow/shrink the selection just as in Edit Mode. [https://projects.blender.org/blender/blender/pulls/105633 PR #105633] [https://projects.blender.org/blender/blender/pulls/105607 PR #105607]
[[File:Extend secletion demo.mp4|720px]]

=== Duration Display in the Status Bar ===
It is now possible to display the scene duration in the status bar.
The format is the following: `Duration: <timecode> (Frame <current frame>/<total frames>)`
The timecode format follows the user preferences. [https://projects.blender.org/blender/blender/pulls/104882 PR #104882]

[[File:Scene duration.png|Scene Duration Display]]

=== Frame Channels Operator ===
Allows to frame onto the contents of a channel, or a selection of channels. Works in the Graph Editor and the Dope Sheet. [https://projects.blender.org/blender/blender/pulls/104523 PR #104523]
It is called with "." on the numpad or right click the channels and choose the "Frame Selected Channels" option.
Alternatively press ALT+MMB to frame the channel under the cursor.

[[File:Blender-3-6-frame-channel-demo.mp4|720px]]

=== Copy Global Transform ===

The Copy Global Transform add-on can now also mirror the transform ([https://projects.blender.org/blender/blender-addons/commit/606e4b35f2 606e4b35f2]).

[[File:Blender-36-copy-global-transform-mirror.png|Options of Copy Global Transform]]

* Object but no bone name: the transform will be mirrored relative to the object
* Object and bone name: the transform will be mirrored relative to that specific bone of that specific armature
* Bone name but no object: the transform will be mirrored relative to the named bone of the current armature

=== Bone Relation Lines ===

Instead of always drawing the relationship line from the head of the child to the ''tail'' of the parent, there is now choice for the parent side of the line. This can be toggled between the ''head'' and the ''tail'' of the bone, where the tail is Blender's original behaviour and is retained as default value ([https://projects.blender.org/blender/blender/commit/45c1deac4f61af39f58678d96cfa7447cea4e4a4 45c1deac4f]).

[[File:Blender-36-bone-relation-lines-options.png|Options of bone relation lines]]

[[File:Blender-36-bone-relation-lines.png|Different bone relation lines]]

=== Context properties ===

[https://projects.blender.org/blender/blender/pulls/105132 PR #105132] Drivers: Introduce the Context Properties

A special type for driver variable has been added: the Context Property.

The property is implicitly referring to either a scene or a view layer of the currently evaluating animation system. This is a weak reference which does not lead to the scene or view layer referenced from the driver to be linked when linking animation data.

An example when such properties comes in play is referring to a transformation of the active camera. It is possible to setup driver in a character file, and make the driver use the set camera when the character is linked into a set.

=== New Parent Space transformation ===
[https://projects.blender.org/blender/blender/pulls/104724 PR #104724] Animation: New parent space transformation aligns child objects / armature bones to parent space. 

[[File:Parent_space_object.png|Child object using parent space transformation gizmo]]

[[File:Blender-36-parent-space-bone.png|Child bone using parent space transformation gizmo]]

== Graph Editor ==

=== Settings moved to the User Preferences ===

Two view options in the Graph Editor have been moved to the User Preferences (Only Selected Curve Keyframes, Use High Quality Display). ([https://projects.blender.org/blender/blender/pulls/104532 PR #104532] Animation: Move Graph Editor settings to User Preferences)

[[File:Moved Graph Editor Settings.png]]

=== Key menu cleaned up ===

The "Key" menu in the Graph Editor has been cleaned up. Things that have to do with channels were moved into the "Channels" menu. The rest has been grouped into sub-menus where possible.
[https://projects.blender.org/blender/blender/pulls/106113 PR #106113] Animation: Clean up "Key" menu in Graph Editor

[[File:Key menu.png]]

=== Gaussian Smooth Operator ===
[https://projects.blender.org/blender/blender/pulls/105635 PR #105635]
A new operator to smooth keyframe data. 
It improves on the current implementation in the following ways.
* Supports modal operations
* Is independent of key density
* More options in the redo panel
* Smooths sudden spikes more predictable

[[File:Gaussian smooth.mp4|720px|Demo of the Gaussian Smooth operator]]

=== Keyframes ===
* Add option to insert keyframes only on the active F-Curve. [https://projects.blender.org/blender/blender/pulls/106307 PR #106307]
* Pasting keys in the Graph Editor now has value offset options. [https://projects.blender.org/blender/blender/pulls/104512 PR #104512]

=== Small Tweaks ===
* When hitting normalize, the y extents of visible F-Curves are frame automatically. [https://projects.blender.org/blender/blender/pulls/105857 PR #105857]
* When the view is normalized, the area outside of -1/1 is drawn slightly darker. [https://projects.blender.org/blender/blender/pulls/106302 PR #106302]
* Ignore hidden curves when jumping to keyframes in the Graph Editor. [https://projects.blender.org/blender/blender/pulls/108549 PR #108549]

== Dope Sheet ==

* Added the pin icon to the channels in the Dope Sheet [http://projects.blender.org/scm/viewvc.php?view=rev&root=bf-blender&revision=B49ad91b5ab7b rB49ad91b5ab7b]
* Clamp the viewport in the Dope Sheet so channels cannot go off screen. [https://projects.blender.org/blender/blender/pulls/104516 PR #104516]

== NLA ==
* Clicking the NLA/Graph Editor search box no longer scrolls the items below it. ([https://projects.blender.org/blender/blender/commit/d1219b727c d1219b727c]).
  [[File:NLA solo button change.png|NLA solo button now right aligned ]]
* NLA track Solo button has been moved on the to be right aligned to group it logically with Mute, Lock track buttons.([https://projects.blender.org/blender/blender/commit/d8024135a5 d8024135a5]).
* NLA strips can now be dragged horizontal through other strips on the same track ([https://projects.blender.org/blender/blender/commit/8833f5dbf9 8833f5dbf9]).
  [[File:NLA horizontal drag and drop.mp4|NLA horizontal drag and drop]]

== Python API ==

=== FCurve key deduplication + exposing the innards of `fcurve.update()` ===

New functions on `fcurve.keyframe_points` ([https://projects.blender.org/blender/blender/commit/85ed2e8c368bb624166d5082df79faa9d983573e 85ed2e8c36]):

- `deduplicate()`: Removes duplicate keys (i.e. when there are multiple keys on the same point in time) by retaining the last key of those duplicates based on their index in the `fcurve.keyframe_points` array.
- `sort()`: Ensure the keys are chronologically sorted.
- `handles_recalc()`: Recalculate the handles, to update 'auto'-type handles.

Calling `fcurve.update()` actually performs `sort()` and then `handles_recalc()`, and now those two steps are exposed as individual functions as well. 

Note that in Blender 4.0 `fcurve.update()` will also be calling the `deduplicate()` function.