= VFX & Video =

== Sequencer ==
* Add filter method to strip transform ([https://projects.blender.org/blender/blender/commit/1c5f2e49b7bf 1c5f2e49b7])
* Use float for transformation offset ([https://projects.blender.org/blender/blender/commit/b4700a13c6ab b4700a13c6])
* Now it is possible to create a new scene base on the active strip. The new scene is assigned to the active strip. ([https://projects.blender.org/blender/blender/commit/2d24ba0210e3 2d24ba0210])
* Add channel headers - provides ability to assign label and per channel mute / lock buttons. ([https://projects.blender.org/blender/blender/commit/277fa2f441f4 277fa2f441])
* Enable edge panning for transform operator - when dragging strip out of view, it pans the view. ([https://projects.blender.org/blender/blender/commit/e49fef45cef7 e49fef45ce])
* Better handling of animation when transforming strips, transforming no longer extends selection. ([https://projects.blender.org/blender/blender/commit/32da64c17e9d 32da64c17e])
* Add frame selected operator for preview. ([https://projects.blender.org/blender/blender/commit/e16ff4132e35 e16ff4132e])
* Thumbnail for first image now represents handle position ([https://projects.blender.org/blender/blender/commit/29b9187b327c 29b9187b32])
* Add option to limit timeline view height ([https://projects.blender.org/blender/blender/commit/17769489d920 17769489d9])
== Compositor ==
* Add Combine and Separate XYZ Nodes ([https://projects.blender.org/blender/blender/commit/9cc4861e6f7 9cc4861e6f])

== Clip Editor ==
* Fix tilt/scale slider of marker jumping on initial mouse grab ([https://projects.blender.org/blender/blender/commit/dd7250efb1a dd7250efb1])
* Fix for tilt/scale slider to follow mouse exactly ([https://projects.blender.org/blender/blender/commit/00018bfaa2a 00018bfaa2])

== Motion Tracking ==

* Fixes very slow tracking with keyframe pattern matching when movie does not fit into cache ([https://projects.blender.org/blender/blender/commit/e08180fdab8 e08180fdab])