= VFX & Video =

== Optical Center in motion tracking ==

The internal storage of the optical center has been changed in the [https://projects.blender.org/blender/blender/commit/7dea18b3aa 7dea18b3aa].

The change allows to change the underlying movie clip resolution (for example, when going from 4K footage to manual 2K proxy) without loosing the non-frame-centered optical center. This effectively fixes an issue when changing the media file would reset the optical center to the frame center.

On a user interface the optical center is now refered in a normalized space: coordinate `(0, 0)` corresponding to the frame center, `(-1, -1)` the left bottom frame corner, `(1, 1)` is the top right frame corner. Another change in the interface is the removed `Set Principal to Center` button. Such dedicated button is not needed anymore since backspace on the optical center properties will do just that.

== Sequencer ==

* Sequence transform has a new filtering mode "Nearest (3x3)". This mode will use "Nearest" filtering when working in the sequencer, but during rendering it will perform a 3x3 sub-sampling, improving the quality of the final image.
* Copy drivers when strips are duplicated or pasted  [https://projects.blender.org/blender/blender/commit/4e9c6929c1d0 4e9c6929c1]. 
* Added "Update Scene Strip Frame Range" operator [https://projects.blender.org/blender/blender/commit/90e940686692060dfa2de55d1391970c1b99003f 90e9406866].