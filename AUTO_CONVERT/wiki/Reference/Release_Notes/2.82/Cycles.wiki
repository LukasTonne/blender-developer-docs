= Blender 2.82: Cycles =

== Denoising ==

Cycles now supports the AI-Accelerated Denoiser from OptiX. It is built into the Blender view layer system, supports multiple GPUs (the denoiser will use the selected OptiX devices also used for rendering) and can give realistic results with low numbers of samples at very high performance.

This denoiser is less suited for animations, because not temporarily stable, but is considerably faster than the existing denoising options and therefore especially useful to denoise previews or final single-frame images with high quality.

<center>
{| class="transparent" style="text-align: center"
 |+  style="caption-side: bottom" | Before and after denoising, with 10 samples
 |valign=top|[[File:Blender2.82_classroom_10_optix_before.png|300px|center]]
 |valign=top|[[File:Blender2.82_classroom_10_optix_after.png|300px|center]]
 |}
</center>

To use this feature you need a NVIDIA RTX GPU and at least driver 441.87 (Windows) or 440.59 (Linux). Currently it can only be used when rendering with OptiX too.

== Shader Nodes ==

Shader nodes for Cycles and Eevee have been extended.

* Map Range node now has interpolation modes : linear, stepped linear, [https://en.wikipedia.org/wiki/Smoothstep smoothstep] and [https://en.wikipedia.org/wiki/Smoothstep smootherstep] ([https://projects.blender.org/blender/blender/commit/958d0d4236b1c 958d0d4236])
* Math node has new operations: trunc, snap, wrap, compare, pingpong, sign, radians, degrees, cosh, sinh, tanh, exp, smoothmin and inversesqrt. ([https://projects.blender.org/blender/blender/commit/0406eb1103 0406eb1103])
* Noise and Wave texture nodes distortion was improved to distort uniformly in all directions, instead of diagonally. This change is not strictly backwards compatible, the resulting pattern is a little different.  ([https://projects.blender.org/blender/blender/commit/074c00f9d6 074c00f9d6])
* Geometry node now has a Random Per Island option, to randomize textures or colors for different components within a mesh. ([https://projects.blender.org/blender/blender/commit/1c2f7b022a 1c2f7b022a])

== Shader AOVs ==

Custom render passes are now supported. They are added in the Shader AOVs panel in the view layer settings, with a name and data type. In shader nodes, an AOV Output node is then used to output either a value or color to the pass.

== Improvements ==

* The Denoising Albedo pass was improved to work better with the OpenImageDenoise compositor node. ([https://projects.blender.org/blender/blender/commit/4659fa5471 4659fa5471])
* The Denoising Normal pass was changed to output in camera space to work better with the OptiX denoiser (when "Color + Albedo + Normal" is selected as denoiser input). ([https://projects.blender.org/blender/blender/commit/d5ca72191c36 d5ca72191c])
* BVH build time on Windows has been significantly reduced by using a better memory allocator, making it similar to Linux and macOS performance. ([https://projects.blender.org/blender/blender/commit/d60a60f0cb d60a60f0cb])
* Direct and indirect light clamping has been changed to clamp per light, instead of for the whole path. Clamp values on existing scenes will need to be tweaked to get similar results. ([https://projects.blender.org/blender/blender/commit/3437c9c 3437c9c])