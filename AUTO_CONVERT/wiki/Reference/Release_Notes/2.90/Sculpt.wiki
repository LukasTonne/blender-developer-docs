= Blender 2.90: Sculpting =

== Multires Modifier ==

Multiresolution sculpting is now fully supported. It is now possible to select the subdivision level to sculpt on and switch between levels, with displacement information smoothly propagated between them. ([https://projects.blender.org/blender/blender/commit/d4c547b7bd8 d4c547b7bd])

[[File:Multires UI.png|600px|thumb|center|Multires Modifier UI]]

=== Unsubdivide and Rebuild subdivisions ===

The Multires Modifier can now rebuild lower subdivisions levels and extract its displacement. This can be used to import models from any subdivision based sculting software and rebuild all subdivisions levels to be editable inside the modifier. ([https://projects.blender.org/blender/blender/commit/f28875a998d f28875a998])

[[File:Unsubdivide demo.mp4|600px|thumb|center|Unsubdivide different meshes with triangles, ngons and non-manifold geometry]]

=== Subdivision Modes ===

Multires can now create smooth, linear and simple subdivisions. This means that any subdivision type can be created at any time without changing the subdivision type of the modifier. Changing the subdivision type of the modifier is now and advance option only used to support legacy files. ([https://projects.blender.org/blender/blender/commit/134619fabbc 134619fabb])

[[File:Subdivs.png|600px|thumb|center|All subdivision modes available in Multires]]

== Tools ==

* The Cloth filter runs a cloth simulation in the entire mesh. It has 3 force modes and the area were the force is applied can be defined by a Face Set. ([https://projects.blender.org/blender/blender/commit/1d4bae85669 1d4bae8566])

[[File:Cloth filter demo.mp4|600px|thumb|center|Cloth filter applying forces to different Face Sets]]

* The Face Set edit operator allows to modify a Face Set. The supported operations are Grow (Ctrl + W) and Shrink (Ctrl + Alt + W). ([https://projects.blender.org/blender/blender/commit/cb9de95d61b cb9de95d61])

== Brushes == 

* The Pose Brush includes two more deformation modes: Scale/translate and Squash/stretch. They can be selected using the deformation property. Pressing Ctrl before starting the stroke selects the secondary deformation mode. ([https://projects.blender.org/blender/blender/commit/77789a19049 77789a1904])

* The Pose Brush has a Face Set FK mode, which deforms the mesh using the Face Sets to simulate and FK rig. ([https://projects.blender.org/blender/blender/commit/3778f168f68 3778f168f6])

[[File:Pose fk 290 demo.mp4|600px|thumb|center|Pose Brush with FK/IK modes]]

* The Pose Brush has a Connected only property. By disabling it, the brush can affect multiple disconnected parts of the same mesh. The distance to consider a part as connected can be tweaked with the Max Element Distance property. [https://projects.blender.org/blender/blender/commit/438bd823714 438bd82371]

* Clay strips now has more predictable pressure/size and pressure/strength curves. Its deformation algorithm was modified to prevent geometry artifacts in large deformations. ([https://projects.blender.org/blender/blender/commit/560a73610b7 560a73610b]) ([https://projects.blender.org/blender/blender/commit/0a1fbfee2ba 0a1fbfee2b])

[[File:Demo clay strips 290.mp4|600px|thumb|center|Clay strips 2.83/2.90 artifacts comparison]]

* The Topology Slide/Relax mode now has two more deformation modes for Slide: Pinch and Expand. They can be selected using the Deformation property. ([https://projects.blender.org/blender/blender/commit/878d191baee 878d191bae])


== Miscellaneous ==

* The Automasking system was redesigned, so brush will not have and start delay when automasking options are enabled in a high poly mesh. ([https://projects.blender.org/blender/blender/commit/e06a346458f e06a346458])

* Boundary mesh detection was redesigned. Now mesh boundaries are properly detected by the automasking system in both meshes and Multires levels. This means that the smooth brush does not longer automasks boundary vertices by default and its meshes and multires behavior now matches. ([https://projects.blender.org/blender/blender/commit/e06a346458f e06a346458])

* Pen pressure modulation is now supported for the hardness property. The modulation can be inverted for achieving higher hardness with lower pressure. ([https://projects.blender.org/blender/blender/commit/69afdf69702 69afdf6970])