= Blender 2.83: EEVEE =

== Render Passes ==

More render passes supported for compositing. ([https://projects.blender.org/blender/blender/commit/be2bc97eba be2bc97eba]) ([[Source/Render/EEVEE/RenderPasses|developer docs]])

* Emission
* Diffuse Light
* Diffuse Color
* Glossy Light
* Glossy Color
* Environment
* Volume Scattering
* Volume Transmission
* Bloom
* Shadow

== Light Cache Update ==
Reflection Probes are now stored in cubemap arrays instead of octahedron maps arrays. This means pre-2.83 files will need to be rebaked and light caches from 2.83 will not be loadable in previous blender versions. ([https://projects.blender.org/blender/blender/commit/7dd0be9554ae 7dd0be9554])


<center>
{| class="transparent" style="text-align: center"
 |+  style="caption-side: bottom" | Rough glossy surface before and after the change
 |valign=top|[[File:Cubearray_before.png|340px|center]]
 |valign=top|[[File:Cubearray_after.png|300px|center]]
 |}
</center>


== Hair Transparency ==
Hair geometry now supports blending with alpha hash and alpha clip mode. Shadow blend modes are now also supported. ([https://projects.blender.org/blender/blender/commit/035a3760afd2 035a3760af])
<center>[[File:Hair_alpha.png|340px|center]]</center>

== Other Changes ==

* A new `High Quality Normals` option has been added to the render settings to avoid low res normal issues on dense meshes. ([https://projects.blender.org/blender/blender/commit/e82827bf6ed5 e82827bf6e])
* A new `Half Float Precision` option has been added to the Image datablock settings adding the possibility (when the option is turned off) to use 32 bits floating point precision when loading float textures. ([https://projects.blender.org/blender/blender/commit/4e9fffc28926 4e9fffc289])
* Sun shadow bias have been fixed and now behave the same as point light shadow bias. User intervention may be required to fix certain scenes. ([https://projects.blender.org/blender/blender/commit/31ad86884cb1 31ad86884c])
* Lookdev now have an option to display the world environment with variable amount of blurring. ([https://projects.blender.org/blender/blender/commit/6e23433c1a74 6e23433c1a])