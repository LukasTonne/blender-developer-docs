= Blender 2.83: Sculpting =

== Brushes ==

* Clay Thumb Brush: new brush to simulate deforming clay with your fingers, accumulating material during the stroke. [https://projects.blender.org/blender/blender/commit/015d5eda884d 015d5eda88]
* Cloth Brush: new brush with a simple physics solver that helps when sculpting cloth. [https://projects.blender.org/blender/blender/commit/4a373afa5fb8 4a373afa5f]
** The mass and the damping properties of the simulation are properties of the brush.
** It has two additional radius control to limit the influence and falloff of the simulation.
** Masked vertices are pinned in the simulation, and it applies the sculpt gravity directly in the solver.
** The Cloth Brush has 7 deformation modes with 2 falloff types (radial and plane).

[[File:Clothbrushdemo.mp4|800px|thumb|center|Cloth Brush]]

* Pose Brush: option to disable the IK anchor point.[https://projects.blender.org/blender/blender/commit/0ab7e321585f 0ab7e32158]
* Clay Strips Brush: option to change the brush tip shape between a square and a circle. [https://projects.blender.org/blender/blender/commit/6ee6a42d10e3 6ee6a42d10]
* All brushes have a hardness property which controls where the falloff starts affecting the brush in relation to the radius. [https://projects.blender.org/blender/blender/commit/ff0124418f42 ff0124418f]

[[File:Clay brush 2.80 - 2.83.mp4|800px|thumb|center|Clay brush 2.80 - 2.83]]

* Layer Brush: the Layer Brush was completely redesigned:[https://projects.blender.org/blender/blender/commit/47f46637be1b 47f46637be]
** No artifacts when alternating between adding and subtracting or when modifying the same area multiple times
** Layer height preview in the cursor
** Multires support for Persistent Base
** Better masking support

[[File:Layer Brush 2.82 - 2.83.mp4|800px|thumb|center|Layer Brush 2.82 - 2.83]]

* Brushes now have separate normal and area radius, enabled by default for Scrape brush. It achieves better behavior when working on curved surfaces and controls how much volume you want to trim. [https://projects.blender.org/blender/blender/commit/df45257ec53c df45257ec5]
* Surface Smooth mode in the Smooth brush. This mode smooths the high frequency detail while preserving the volume. [https://projects.blender.org/blender/blender/commit/a218be308032 a218be3080]

[[File:Surface Smooth Brush.mp4|800px|thumb|center]]

* Mesh boundary Automasking Option to protect the boundaries of the mesh when sculpting cloth. This option has an Propagation Steps property to control the smoothness of the falloff near the edges of the Sculpt. [https://projects.blender.org/blender/blender/commit/84b94f9e7b8b 84b94f9e7b]

== Face Sets ==

New system to control the visibility state of the mesh in sculpt and paint modes. They are designed to work in modes where brushes are the primary way of interaction and they provide much more control when working with meshes with complex shapes and overlapping surfaces. [https://projects.blender.org/blender/blender/commit/38d6533f212b 38d6533f21]
* Remesher reprojection support. The visibility state of the mesh is also preserved when remeshing.
* Draw face sets tool to create and edit the face sets. Start a new stroke to create a new Face Set. Press Ctrl to modify the Face Set under the cursor and Shift to relax the boundary of a Face Set
* W key pie menu with the most common Face Set visibility operations
* Mask expand operator support. User Shift + W to expand a new Face Set by topology.
* H to toggle Face sets visibility, Alt + H to show all Face Sets, Shift + H to hide the Face Set under the cursor.
* Pressing Ctrl when expanding Face Sets or Masks in the mask expand operator flood fills a topology connected area.



Face set compatibility was also added to some of the existing tools:
* The Mesh Filter can apply a filter to an individual face set with the option "use Face Sets*
* There is a Relax Face Sets filter in the Mesh Filter, which smooths the boundary of all Face Sets [https://projects.blender.org/blender/blender/commit/0dfb4ac1ff9a 0dfb4ac1ff]

[[File:Relax Face Sets Mesh Filter.mp4|800px|thumb|center]]

* The Pose Brush can snap the rotation origin points to the boundaries of the Face Sets [https://projects.blender.org/blender/blender/commit/9120191fe2a2 9120191fe2]

[[File:Pose Brush Face Sets origin mode.mp4|800px|thumb|center]]

* Face Set automasking option to affect only the Face Set were the stroke started
* Face Set boundary automasking option to avoid affecting the boundaries of the Face Sets [https://projects.blender.org/blender/blender/commit/7c88968c8954 7c88968c89]

== Voxel Remesh ==
* Voxel Size edit operator to change the voxel size of the voxel remesher with a real time preview in the viewport by pressing Shift + R [https://projects.blender.org/blender/blender/commit/1c1a14dcdfff 1c1a14dcdf]
* Voxel mode in the remesh modifier which produces the same result as the Voxel Remesher [https://projects.blender.org/blender/blender/commit/6c036a65c974 6c036a65c9]

== Other ==
* Automasking options can now be enabled globally instead of per brush. [https://projects.blender.org/blender/blender/commit/1f745e2c72b2 1f745e2c72]
* Delay Viewport Updates option to improve the performance when navigating in high poly meshes. When this option is enabled only the data will be updated in the viewport when the view navigation stops, avoiding the lag. [https://projects.blender.org/blender/blender/commit/b8d9b5e33152 b8d9b5e331]
[[File:Delay Viewport Updates.mp4|800px|thumb|center]]
* Surface Smooth mode in the Mesh filter to remove high frequency detail while preserving the volume
* Sharpen mode in the Mesh Filter which pinches the edges and smooths flat surfaces automatically
[[File:Sharpen Mesh Filter.mp4|800px|thumb|center]]