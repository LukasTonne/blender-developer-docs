= Blender 2.91: Python API =

== Compatibility ==

* The Principled BSDF shader has a new Emission Strength input. Material importers and exporters need to be updated to take this into account.  ([https://projects.blender.org/blender/blender/commit/b248ec9 b248ec9])

== Scene Ray Cast ==

The first parameter to the `scene.ray_cast()` function has changed ([https://projects.blender.org/blender/blender/commit/e03d53874dac5f e03d53874d], [https://developer.blender.org/rBA82ed41ec632483fa9260d90dae7afdf3192c509b A82ed41ec63]). It now receives a Depsgraph instead of a View Layer. This made it possible to [https://developer.blender.org/T79127 fix a crash], and also fixes potential issues when ray-casting is done while rendering (for example from a custom driver function).

<source lang="Python">
# Old call example:
context.scene.ray_cast(context.view_layer, ...)

# New call example:
context.scene.ray_cast(context.view_layer.depsgraph, ...)
</source>

The other parameters and the return value has remained the same.

== Other changes ==

* Add `ghost` argument to `Struct.is_property_set`, so it's possible to test if an operator's property is being reused from the last time it was called ([https://projects.blender.org/blender/blender/commit/d3bcbe10c20e8b d3bcbe10c2]).
* `wm.read_homefile` now has a `use_factory_startup` argument, which can be used as a faster alternative when resetting the file contents for tests since `wm.read_factory_settings` resets preferences (re-registering all add-ons) ([https://projects.blender.org/blender/blender/commit/cf67ba848f2f42e0b7320aa0654ba89bd14a1416 cf67ba848f]). 
* Add `UILayout.introspect()` for scripts to access the UI layout (for testing/debugging) ([https://projects.blender.org/blender/blender/commit/428a1aaf7372aaad793fe7cc03128db18e3ae602 428a1aaf73]).
* New `Mesh.attributes` API for accessing custom geometry attributes on meshes. This replaces `vertex_layers_float`, `vertex_layers_int`, `vertex_layers_string`, `polygon_layers_float`, `polygon_layers_int` and `polygon_layers_string`, which are now deprecated. Instead `.attributes` provides a single list of geometry attributes for all combinations of geometry element and data types. Further, the data types have been extended with 3D Float Vector, Byte Color and Float Color. ([https://projects.blender.org/blender/blender/commit/565510b 565510b])
* New `VolumeGrids.save()` function to save volume grids and metadata to disk. ([https://projects.blender.org/blender/blender/commit/fc76750 fc76750])
* Custom property names can no longer contain `"`, `'`, or `\`. These characters can cause problems with drivers and Python and may not have worked properly in the past. ([https://projects.blender.org/blender/blender/commit/cbae82ba960a cbae82ba96])
* Actions are bound to the type of the datablock they're assigned to; a Camera Data action cannot be asigned to an Object. This "locking" used to happen when the animation was evaluated; now it happens when the Action is assigned. ([https://projects.blender.org/blender/blender/commit/64583f3e8d0f 64583f3e8d])
* `sys.executable` now points to the Python interpreter (instead of the Blender executable) ([https://projects.blender.org/blender/blender/commit/04c5471ceefb41c9e49bf7c86f07e9e7b8426bb3 04c5471cee]). This resolves `multiprocessing` which failed to spawn new processes on WIN32.
* `bpy.app.binary_path_python` has been deprecated (use `sys.executable` instead).
* Since porting `bpy.ops.uv.smart_project` to C ([https://projects.blender.org/blender/blender/commit/850234c1b10a 850234c1b1]), `angle_limit` has to be given in radians.