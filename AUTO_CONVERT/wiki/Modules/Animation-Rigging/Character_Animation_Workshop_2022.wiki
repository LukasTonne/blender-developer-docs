= Character Animation workshop (2022) =

The goal of this three-day workshop will be to come up with a design for a new character animation system in Blender. It'll be the kickoff of a 3-year project ''Animation 2025''.

== Presentation and Results ==

The presentation is available on YouTube: [https://www.youtube.com/watch?v=msOjFC03JRY The Future of Character Animation & Rigging: a Q&A feeback session]

* [https://docs.google.com/document/d/1qjD1MhXAFnnySSzey9DW-vumhtNavUxqCbBM9P30FzQ/edit Design Principles]
* [https://docs.google.com/document/d/11_7rUJ-UJXH9vEMHHbq4oRAOkm71hjR9INcrKNWpB9E/edit What's stopping you from animating in Blender?]
* [https://docs.google.com/document/d/1fvxezZJHHHsa7bcufdRFpWyrqjQuVHqxNTUc5Sg0VCI/edit Blank Slate - Character Animation in Blender]
* [https://download.blender.org/institute/sybren/animation-rigging/2022-character-animation-workshop/2022-animation-workshop-mural-download.pdf PDF of the Mural workboard used during the workshop] (12½ MB)

[[File:2022-animation-workshop-big-blocks.webp|Bigger topics for planning|thumb|center]]

== Dates & Locations ==

'''Date:''' 24-26 October 2022

'''Location:''' [https://www.blender.org/about/institute/ Blender Institute]

'''Remote:''' The main communication channel is [https://blender.chat/channel/animation-module #animation-module on Blender Chat]. Video meeting will happen via [https://meet.google.com/ora-nbxv-evf Google Meet on the usual meeting ID].

== Show & Tell ==

If you're joining the workshop, or even if you just want the workshop-peoples to see something you think is important (max 5 minute video), '''please use [https://docs.google.com/forms/d/e/1FAIpQLSey84FhfB6yx-bf_GMqIgiIwhy05W-ystMfb_LVLLMA-U_HuA/viewform this sign-up sheet]'''. It'll help us streamline the process.

== Schedule ==

Every day, there will be three "sync moments" to make sure online/remote attendees are in sync with what's happening at Blender HQ, and vice versa.

All times are in local time, so CEST / Amsterdam time.

* 10:00 morning kickoff
* 14:00 after lunch
* 18:00 end of day

== Documents ==

* [https://docs.google.com/document/d/1qjD1MhXAFnnySSzey9DW-vumhtNavUxqCbBM9P30FzQ/edit Design Principles]
* [https://docs.google.com/document/d/11_7rUJ-UJXH9vEMHHbq4oRAOkm71hjR9INcrKNWpB9E/edit What's stopping you from animating in Blender?]
* [https://docs.google.com/document/d/1fvxezZJHHHsa7bcufdRFpWyrqjQuVHqxNTUc5Sg0VCI/edit Blank Slate - Character Animation in Blender]
* [https://docs.google.com/spreadsheets/d/1dBNl54Oz4X4S21z83jBFWlAOWTUQ8KPUAmZcxBhkBf4/edit Workshop schedule]
* [https://docs.google.com/forms/d/e/1FAIpQLSey84FhfB6yx-bf_GMqIgiIwhy05W-ystMfb_LVLLMA-U_HuA/viewform Sign-up Sheet] ([https://docs.google.com/forms/d/1xVXAszg1wfFj4bcKYxIYkca5rc7pe88Mf7RpNSI_-r0/edit?usp=drive_web edit])
* [https://app.mural.co/invitation/mural/home4643/1665674871684?sender=u6f41ad2ad961ab9f6e6a1021&key=91496bec-547f-44bc-a122-3ba0e5d32613 Mural, which served as a work/brainstorm board during the workshop]