Notes and documentation about the Sculpt-Paint & Texture module.

=== Subpages ===

* [[Modules/Sculpt-Paint-Texture/Future Projects|Future Projects]]: Bigger projects and opportunities to work on next, or define further.
* [[Source#Sculpt.2C_Paint.2C_Texture|Code Documentation]]: Get started with the source code of the module.
* [[Modules/Sculpt-Paint-Texture/Get Involved|Get Involved]]: Info on contributing with code patches, documentation, education content & feedback.
* [[Modules/Sculpt-Paint-Texture/Weak Areas|Weak Areas]]: Known issues and problems that aren't currently in discussion.