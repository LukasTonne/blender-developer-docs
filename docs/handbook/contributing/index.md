# Contributing Code

When contributing code to Blender, it first ought to be reviewed. This
is mandatory when you do not have the right to commit your changes
directly, but even for seasoned Blender developers it can be beneficial
to have an extra pair of eyes to go over their code.

This wiki page describes the process of code contribution and review.
For specific steps, see the guide to [creating pull
requests](pull_requests.md).

## Before Starting to Code

If you plan to work on bigger changes or new features, it's usually a
good idea to [Contact Developers](../communication/index.md) in the area you
plan to work in, and figure out if the feature is desired and how it
should be designed, before spending time coding it. By talking to
developers first, you can make sure that your work aligns with other
plans, and that the approach is sound.

If you are contributing a small feature or fix, you can usually just go
ahead and create a pull request directly.

## Guidelines

Making contribution is very welcome! To make timely and [ proper
reviews](pull_requests.md) possible, we'd recommend you to
check on the guidelines below.

- Format your commit message following the [style
  guidelines](../guidelines/commit_messages.md).
- Make a separate comment or use a horizontal separator (`---`) in the
  description for additional information that is not intended to be in
  the commit message.
- Follow [**code guidelines**](../guidelines/index.md): no new warnings,
  comment your code, add documentation, follow code style of the file
  you are editing.
- Try to keep everything in a pull request related to **one topic**. If
  there are two things you're fixing, e.g. adding a new hotkey and
  fixing a memory leak, submit two separate pull requests, one for each
  topic.
- Include all the **Ingredients of a Pull Request** in the description
  (see the section below).
- Contributions solving bugs or compiling errors need similar
  information as for bug reports, on **how to reproduce** the issue that
  is solved.
- If there are **bigger design question** to solve, open a Design task
  first, so that code review can focus mostly on the implementation.
- A review has to be done both on **functional** level (does this fit
  with Blender's design or roadmap) and **technical** level (does the
  provided code conform to our coding style, match design and planning,
  and is sufficiently readable and maintainable). It is fine to submit a
  rough PR marked as `WIP:` first to demonstrate the functional level,
  and then update it so that it is also functionally sound. Also make
  this clear in the description.
- Be explicit about **who should maintain** the new code. For larger
  changes it's relevant to note if you seek one of the Blender project
  members to maintain it, or whether you propose to get involved as team
  member to maintain/improve it in the future.
- Provide as much **relevant documentation** as possible. Images,
  screenshots, short video clips or .blend file examples do do wonders!
  You can attach all of this to your submitted pull request on
  [projects.blender.org](https://projects.blender.org/).
- The typical reviewers with **final approval** should be module owners,
  but anyone can and is encouraged to participate.
- Make it easy for people to review the code. That means you **do not
  submit huge changes**, but break up your work into multiple smaller
  pull requests and reviews.
- Code refactoring, new features, bug fixes, etc. should generally be
  submitted as **separate code reviews**. You can have them as separate
  commits in the same local Git branch.
- Code reviewers should try to either accept, request changes, or
  reassign to another reviewer quickly, to unblock the developers
  working on their features. This makes the process more inviting to new
  developers and more usable for existing developers.

## Ingredients of a Pull Request

Before adding a new feature (even a small one), it needs to be discussed
and motivated. As a minimum, the description should include the
following where applicable:

1.  Description of the problem that is addressed.
2.  Description of the proposed solution, and a motivation as to why
    this is the best solution.
3.  List of alternative solutions, and a motivation as to why these are
    undesirable.
4.  Limitations of the proposed solution.
5.  Mock-up of the proposed user interface and a description of how
    users are going to interact with the new feature.

These ingredients don’t have to be full essays. A few well-written
sentences can be very illuminating as well. An example can be found in
[D7533](https://developer.blender.org/D7533). A simpler example that
still adheres to the above can be found in
[D6352](https://developer.blender.org/D6352).

The order in which these are listed is also relevant, as it helps to
guide the reader towards understanding what the change is doing. To give
a counter-example, starting a description with “This drop-down menu
allows the artist to…” can be hard to understand at first, as the reader
doesn’t know anything about the underlying problem yet.

Every day developers have to dive into Blender's history to find
rationales on changes. When this results in a commit or pull request
with no rationales at all, developers end up having to make guesses and
assumptions, which tend to backfire and result in bugs. The alternative
is to spend a significant amount of time understanding the changes and
testing corner cases. By writing down some (brief) background
information and rationales for a change, it helps to make better, more
informed decisions later.

## Finding Reviewers

To find out who exactly is responsible for some area of the code you can
check the [modules
list](https://projects.blender.org/blender/blender/wiki) or [browse the
code](https://projects.blender.org/blender/blender) and history for each
file to see who worked on similar files or who is the author of an
add-on. When you are working on bigger changes, ideally you'd first
[contact developers](#before-starting-to-code) and talk about
your ideas. This will already give you an idea as to who will be
suitable for reviewing your code.

Doing a good review can take a lot of time, and because many Blender
developers are volunteers, we cannot make a promise you'll get such a
review here! Below are tips on how to get reviews done. To get
developers to review your pull request, you can contact them in various
ways:

- Go on \#blender-coders on [blender.chat](https://blender.chat) and
  talk to developers.

> WARNING: **Important**
> When submitting a pull request, please only add developers or modules
as reviewers if you discussed this with them before. Otherwise please
mention suitable modules or developers about your pull request in a
comment first.

When the pull request has actual new functionality, user reviews will
help a lot too. Many changes are tested first by making a build for the
[experimental builds](https://builder.blender.org/download/patch/) page.
Other changes could benefit from discussion on the
[devtalk](https://devtalk.blender.org/) forum before proper code review
begins.

## Help, my pull request got rejected!

Even for experienced developers, it is very common that reviewers do not
accept a pull request immediately. When you send in a pull request,
please expect that you'll be asked to change things around. After all,
that's what reviews are for -- to find weaknesses and solve them.

We're all software developers here, and we understand code work on
Blender shows a lot of commitment, and has been your time investment
with all the good intentions to help us out. A rejection doesn't mean we
don't like you, nor that we don't like to see actively helping out as a
contributor! It's all human beings here, with personal preferences, and
ideas for how to make Blender a better product.

It makes Blender as an open source project strong if we allow active
developers and maintainers to make individual choices too. They're
getting empowered to do development, which also implies they have get
the benefit of the doubt in making tough decisions.

Nevertheless, mistakes can always happen! Here's what to do to get a
contribution considered after all:

- Contact the developer chat with a friendly request for (additional)
  reviews. Make sure the current maintainer of code at least is aware of
  this rejection.
- If no result or consensus in the project team can be found, you can
  ask the Blender [admins](../organization/index.md#admins) to
  provide a final verdict.
