# Snap Store

Blender is available at <https://snapcraft.io/blender>.

The `main` branches is available as `latest/edge`, and betas and release
candidates are available as well. See the instructions on the web page
for how to install them.

Delivery is automated through the buildbot.
