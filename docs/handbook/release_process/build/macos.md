# macOS Libraries Maintenance

## ARM

The basic thing to build libraries is to run `make deps`, which will
download and compile all libraries in `../build_darwin/deps`.

Most likely it will tell you that you are missing some tools, and give
you homebrew command to install them.

After this is done it will automatically install the updated libraries
to `lib/darwin_arm64` from where they can be committed.

However note that it will likely update all libraries even those that
had no change, as we do not have reproducible builds. So `svn
revert` parts that effectively stayed the same.

## x86\_64 Cross Compilation

x86\_64 libraries are currently cross compiled on ARM, which is
convenient to have on a single machine but it is a kludge.

### Homebrew

This requires two homebrew installs, one for arm and one for x86\_64.
They will be in different folders automatically.

macOS supports dropping into a x86\_64 terminal like this.

``` bash
arch -x86_64 zsh
```

From here you can install homebrew a second time.

Then add the following in `~/.zshrc`, so it automatically picks
up the right homebrew install depending on the arch.

``` bash
ARCH=$(arch)
if [[ $ARCH == "i386" ]]; then
  eval "$(/usr/local/bin/brew shellenv)"
else
  eval "$(/opt/homebrew/bin/brew shellenv)"
fi
```

This probably replaces some existing code put there by homebrew.

### Building

So always drop into the x86\_64 terminal first.

``` bash
arch -x86_64 zsh
```

Setting up the build should be something like this, modify paths as
needed:

``` bash
cmake -DHARVEST_TARGET=/path/to/lib/darwin -DCMAKE_OSX_ARCHITECTURES=x86_64 -S blender/build_files/build_environment -B build_darwin/deps_x86
cd build_darwin/deps_x86
make -j16 install
```

## LTS and CVEs

We committed to updating libraries when security issues for them are
found, both for main and 3.3 LTS.

Checking for such issues is automated by `make cve_check` in the
libraries build folder.

If you have enough disk space you may consider setting up an entirely
separate build and lib folder for 3.3 since switching with main is
painful, as you will need to clear the entire build folder for it to
rebuild correctly.

## Committing a Subset of Libs

``` bash
svn changelist -R tmp *
svn changelist -R --remove library_a library_b
svn revert -R --changelist tmp .
svn changelist -R --remove --changelist tmp .
svn status
```
