---
hide:
  - navigation
  - toc
  # Hide Preview/Next footer.
  - footer
---

# License

Blender itself is released under the [GNU General Public
License](https://www.gnu.org/licenses/gpl-3.0.html). More info
[blender.org/about/license](https://www.blender.org/about/license/).

Except where otherwise noted, the content of the Blender Developer Documentation
is available under a [Creative Commons Attribution-ShareAlike 4.0 International
License](https://creativecommons.org/licenses/by-sa/4.0/) or any later version.
Excluded from the CC-BY-SA are also the used logos, trademarks, icons, source
code and Python scripts.

Please attribute the “Blender Developer Documentation Team” and include a
hyperlink (online) or URL (in print) to manual. For example:
> The [Blender Developer Documentation](https://developer.blender.org/docs) by
the [Blender Developer Documentation
Team](https://projects.blender.org/blender/blender-developer-docs) is licensed
under a [CC-BY-SA v4.0](https://creativecommons.org/licenses/by-sa/4.0/).

See [Recommended practices for
attribution](https://wiki.creativecommons.org/wiki/Recommended_practices_for_attribution)
for further explanation.

It means, that when contributing to the documentation you do not hold exclusive
copyright to your text. You are, of course, acknowledged and appreciated for
your contribution. However, others can change and improve your text in order to
keep the manual consistent and up to date.

If you have questions about the license, feel free to contact the Blender
Foundation: foundation (at) blender (dot) org