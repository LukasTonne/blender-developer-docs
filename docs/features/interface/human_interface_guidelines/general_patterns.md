# General Patterns

- **Try to establish a left-to-right, top-to-bottom reading flow**.
  Arranging elements this way ensures the flow of information feels
  natural for the user, because it follows what they are used to from
  text. For right-to-left languages it can (or should even) be right to
  left, top to bottom.

## Editors

- **If the exact coordinates of items in an editor matter, the editor
  should draw a grid background** (or a grid floor in a 3D editor). For
  example the UV Editor or Graph Editor. This grid can go in one
  direction only if there is only one variable axis. For example the
  Dopesheet displays grid lines along the horizontal axis to represent
  frames, but the vertical axis represents different animation channels,
  which keyframes can't be moved between.
- **If the exact coordinates of items in an editor with scrolling on two
  axes does *not* matter, the editor should draw a dotted grid**. For
  example the Node Editor. The grid only serves as a reference point for
  view transforms, so you can see the view scroll even if there is no
  content.
