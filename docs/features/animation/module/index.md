# Animation & Rigging

Notes and documentation about the Animation & Rigging module.

**Subpages:**

- [Bigger Projects](bigger_projects.md): broad ideas for the future.
- [Code Documentation](../index.md)
- [Code Review](code_review.md)
- [Weak Areas](weak_areas.md): areas   of the Animation & Rigging system that could use improvement.
- [Character Animation Workshop 2022](character_animation_workshop_2022.md)
