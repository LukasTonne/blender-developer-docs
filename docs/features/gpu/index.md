# GPU Module

The GPU module is an abstraction layer between Blender and an Operating System
Graphics Library layer (GL). These GLs are abstracted away in GPUBackends.

There is a GLBackend that provides support to OpenGL 3.3 on Windows and Linux.
There is also a Metal backend for Apple devices. Vulkan backend is currently
in development.
