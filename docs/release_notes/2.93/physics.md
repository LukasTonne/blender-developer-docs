# Blender 2.93: Nodes & Physics

## General

- Add muting support for node wires
  (blender/blender@266cd7bb82ce)
  - To mute a wire, drag over it while holding `Ctrl + Alt`
