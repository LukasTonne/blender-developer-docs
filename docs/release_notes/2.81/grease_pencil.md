# Blender 2.81: Grease Pencil

## User Interface

- Edit Mode Menu reorganization: The old single stroke menu was divided
  into Grease Pencil, Stroke and Point menu.
  (blender/blender@810caad80e6f)
- Edit Mode Context Menu reorganization: The old single context menu was
  divided into Stroke and Point menu.
  (blender/blender@8c7cbad54274)
- Draw Mode Menu updates
  (blender/blender@0067b3b09b1c)
- Mask Selection in Sculpt Mode now support modes for selection (point,
  stroke, In between) Separately from Edit Mode selection modes.
- Stroke placement now use smaller numbers to determine the offset
- Object data updates: Opacity and Blend controls have been changed for
  consistency, Mask control is accesible now only at layer list and
  *Show only On Keyframed toggle* was moved to Display Layer section.
  (blender/blender@d795dd1fa780)
- Paste to active layer by default and change names.
  (blender/blender@51d9f56f874d)
- When set Stroke Select Mode in Edit, the edit points are not displayed
  and the current selection is extended to the full stroke for any
  selected stroke.
  (blender/blender@6b33bd1067dc)
- Overlays: a new property to include Grease Pencil objects to the *Fade
  3D Object* control was added. Name was changed to *Fade Object* to
  reflect the change.
  (blender/blender@a5a003ed5589)

![The non-active Grease Pencil objects and meshes faded](../../images/Fade_Gpencil_object.png){style="width:600px;"}

- Overlays: *Fade layers* using background color and available in all
  modes.
  (blender/blender@74dcfaf1722f)
- Menu items order in *Arrange Menu* changed to make more consistent
  with the distance.
  (blender/blender@e164afe9b5f9)
- New Simplify Layer Tinting to disable all tints in one step.
- New postprocesing Simplify option for brushes.

## Operators

- New *Merge by Distance* operator (Clean Up Menu).
  (blender/blender@b9d0f33530f0)
- New *Simplify Sample* operator.
- Improved algorith for the *Smooth Operator* when using Thickness and
  Strength.
  (blender/blender@0e1d4dec7a7d)
- New option to convert Curves to Grease Pencil strokes.
  (blender/blender@505340202e96)

![Convert Curves to Grease Pencil operator](../../images/Convert_Curves_to_Grease_Pencil_operator.png){style="width:600px;"}

- New *Set as Active Material* to establish the active object material
  based on the selected stroke material.
  (blender/blender@d0462dca9071)

## Tools

- Improvement in *Guides*: Better snapping, new ISO grid option for
  lines specified by Angle, radial snapping mode uses Angle as an offset
  (blender/blender@a94bf0e1349b)

![Isometric Guides sample (45°)](../../images/ISO_guides.png){style="width:600px;"}

- Improvements in Strokes ending.
  ([b393b135245e](https://developer.blender.org/rBb393b135245e))

![Better strokes ending when drawing lines in a fast way](../../images/Strokes_ending.png){style="width:600px;"}

- Improvements when move mouse/pen very fast - Smart Smooth.
  ([12688a6636da](https://developer.blender.org/rB12688a6636da))
- New Brushes and presets.
  (blender/blender@3c7707b49fc6)

![New Brushes and presets](../../images/New_Brushes.png){style="width:600px;"}

## Modifiers

- Move Modifiers evaluation from the Draw Manager to the Depsgraph.
  (blender/blender@ee4ec69b2804
  blender/blender@091e7979d314
  blender/blender@a67f218b5417
  blender/blender@2dd1e6b376bf)
- Improved *Noise Modifier* with a new seed value.
- Grease Pencil Modifiers have a new influence filter by a single
  material
  (blender/blender@3d8f1586973b)
- *Simplify Modifier* has two new modes: Sample and Merge
- *Opacity Modifier* has a new Strength mode
  (blender/blender@591db72ee2ee)

## Materials

- New *Self Overlap* parameter in materials to disabled the stencil
  effect on the strokes.

![Self Overlapped stroke and Stencil stroke](../../images/Self_Overlap.png){style="width:600px;"}

- New *Default Material* for strokes when material slots are empty
  (blender/blender@d5d3dc418912)
