# Blender 2.81: Add-ons

## Add-on Preferences

- A checkbox "Enabled Add-ons Only" was added to the add-on list. When
  checked, only the enabled add-ons are shown; un-check to show all
  installed add-ons. This replaces the 'Enabled' and 'Disabled' items in
  the categories filter.
  blender/blender@078d02f55743cd34c5

## Add-on Preferences: New Add-on Category List

- During the transition from 2.80 to 2.81, there has been significant
  changes to Add-ons and the categories they appear in. Some of the
  addons may have moved to a new category. The 3D View category has been
  limited to operations on the 3D Viewport such as overlays and a new
  category "Interface" created. Interface deals with addons that are UI
  based. The Pie menus addon has been moved to Interface. The category
  list is refined in other areas to better group Add-ons and reduce ui
  clutter.

## Add-on Tabs and Panels

- Most Add-ons included in Blender 2.81 have their panels closed by
  default to avoid ui clutter. We have also changed the tab naming
  conventions to better integrate addons into the ui. A decision was
  made that add-ons should not use the Active Tool tab unless it's a
  modal operator.
- Add-ons have been grouped into the Item, View and custom Edit, Create
  and Animate tabs with the exceptions of a few use specific addons
  using custom names. If you enable all addons in order with the
  exception of the custom names, then enable the custom names, switching
  between object modes has a significantly reduced Tab "jumping" when
  using addons.

<https://developer.blender.org/T70017>

## glTF 2.0 IO

### Importer

- Meshes
  - manage mode other than 4 (manage edges & points without faces)
    blender/blender-addons@7565f120d54c
  - Big performance improvment
    blender/blender-addons@06bb353c8489,
    blender/blender-addons@a0c33fe957a9,
    blender/blender-addons@ba3c5b0b3b82,
    blender/blender-addons@96036641899e,
    blender/blender-addons@eba6fe23d3bb,
    blender/blender-addons@f9e25350dc87
- Animation
  - better multi object animation management
    blender/blender-addons@87ffe55fd5f7

### Exporter

- Materials
  - Check that textures have valid image
    blender/blender-addons@2c08beb9690c
  - Add alpha support
    blender/blender-addons@1e20236039c8,
    alpha support in vertex color
    blender/blender-addons@26c53aa581b0
  - Normal export fix when there are normal modifier(s)
    blender/blender-addons@3c3c2243dbbd
  - Performance
    blender/blender-addons@a9283e526fea
  - Better texture transform mapping management
    blender/blender-addons@42f1e69458db,
    blender/blender-addons@d8e78e3cdcd2,
    blender/blender-addons@8953c208ad1b
- Animation
  - Sample animations is now default to True
    blender/blender-addons@c402af6e27ca
  - Fix skinning export
    blender/blender-addons@7307a3c57d84
  - Fix first and last tangent data for bezier interpolation
    blender/blender-addons@0f85dace7664
  - Take into account NLA tracks for shapekeys animation
    blender/blender-addons@7f732e373a23
  - Various sanity checks
    blender/blender-addons@72599842ab5b,
    blender/blender-addons@5db869653a60,
    blender/blender-addons@ffcecad3a890,
    blender/blender-addons@46b1ada7f5f2,
    blender/blender-addons@4e69d972bd4f,
    blender/blender-addons@525afbadafbe
  - Fix ShapeKeys animation export
    blender/blender-addons@fc320ea236c7,
    blender/blender-addons@dcd48a616b22,
    blender/blender-addons@154758b5a3d8
  - Better multiple animation export (including merging animations)
    blender/blender-addons@9a7d0db875af,
    blender/blender-addons@14f1c99e96ad
  - Performance
    blender/blender-addons@8d9e3c94aff9,
    blender/blender-addons@812cb318c4e0
- Object
  - export object from linked library
    blender/blender-addons@ade13102347c
  - Export custom light ranges
    blender/blender-addons@c49ac6dee0e7
- General
  - Better blender version handling
    blender/blender-addons@50394a12df3f,
    blender/blender-addons@54e504f32b1a
  - Set main scene
    blender/blender-addons@ea5f0df783b6
  - UI change
    blender/blender-addons@12af8a28c14b

## FBX IO

- Crease and sub-surface informations are now supported on both
  exporting and importing ([D4982](http://developer.blender.org/D4982),
  blender/blender-addons@f1dd37b8ac8f).
- Importer can now batch-import several FBX files at once
  ([D5866](http://developer.blender.org/D5866),
  blender/blender-addons@b57772a8831e).
- After the change to the shader's mapping node
  (blender/blender@baaa89a0bc5),
  the min/max feature being removed, the handling of U/V clamping of FBX
  textures has been modified in the importer. It now follows the same
  behavior as the exporter, FBX's individual clamping/repeat on the U or
  V axes are not supported anymore (if any one of those is set,
  Blender's Texture node Extension is set to 'repeat').

## Import Images as Planes

- The default material shader now uses the Principled node, which allows
  to also automatically support transparency, and exports directly in
  several IO formats (GLtf, OBJ, FBX, ...,
  [D5610](http://developer.blender.org/D5610),
  blender/blender-addons@a215a3c85ad9).

## BlenderKit

- A right click menu in the asset bar offers several new (and old)
  functions :
  - Search similar - searches for similar assets
  - Swap model (only for models) - swaps the selection for the new
    assets
  - Delete (own assets).
- Upload has been improved, with possibility to upload thumbnail and
  main file separately
- More UI options, mainly thumbnail size
- The code of the addon now runs on timers, which enables it to
  append/link assets also when the asset bar is hidden.

## Rigify

A big internal API refactoring that has been worked on since last year
has been committed.
(blender/blender-addons@3423174b37,
blender/blender-addons@8b1df84370,
etc)

### For users

- Refactored rigs generate nearly instantly compared to before. Note
  that the face rig has not been updated because it is planned to be
  replaced in the future, and is still slow.
- The Rigify Animation Tools panel has been deprecated and disabled for
  newly generated rigs. Buttons for updating action keyframes have been
  moved to the rig-specific Rig Main Properties panel, next to the
  original IK\<-\>FK snap buttons.
- IK controls have much more sophisticated support for switching the
  active parent: choices between root, torso, hip, etc; operators to
  change parent without moving control, etc.
- The super_spine rig has been split into separate head, middle and tail
  parts; the old combined spine is now just a compatibility wrapper.

### For scripters

- Rig classes have a [new API](../../features/animation/rigify/index.md) for
  interacting with Rigify core, designed to support complex rig
  interaction and subclassing.
- Old rig classes still work via an automatic compatibility wrapper,
  except if they rely on being in Edit mode inside `__init__`.
- New utilities for generating keyframe bake operators specific to the
  rig in the rig UI script, instead of the old Rigify Animation Tools
  panel hardcoded in the add-on to deal with the 'standard' metarig and
  nothing else.
- A number of other new small utilities in the library.

These changes were designed to allow taking full advantage of the
capability to implement and install custom rigs (available since 2.80)
via feature set packages ([feature set
example](https://github.com/angavrilov/angavrilov-rigs/), [legacy rig
feature set](https://github.com/angavrilov/rigify-legacy-rigs)).

## Pie Menu Add-ons

- As Blender 2.8 brought about many pie menus as default menus, the
  "Official" pie menu addon has been depreciated. The Viewport Pies
  add-on is still available as an alternate pie menu system. It has also
  been updated to respect the Workspace Filter Add-ons and several
  individual menu fixes. You can now find it in the interface category
  when you enable addons.

## Dynamic Context Menu

- The dynamic context or "spacebar" addon has had a rework for the 2.8
  series. Originally designed as as a replacement for the legacy 2.49
  spacebar menu, then updated up to 2.79 release, this addon had evolved
  again to adjust to the new Blender tools and interface.
- Access Search, Tools, Animation, Quick Favorites in the one place.
  Collections added. Menu's are reworked and updated to better reflect
  built in 2.8 series tools.
- Note: If you have trouble accessing the add-on in the ui, in
  Preferences \> Keymaps \> Spacebar Action, you may need to set it to
  tools and save your preferences.

## Materials Utils

- Updated from 2.79, the Materials Utils addon makes a return in 2.81.
  Access your materials in the 3d viewport and assign, search, select,
  copy and more.

## Animall

- Updated from 2.79, the Animall addon makes a return in 2.81 thanks to
  Damien Picard (pioverfour). animate mesh, lattice and curve data.
