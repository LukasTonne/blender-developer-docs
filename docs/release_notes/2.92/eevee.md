# EEVEE

## Cryptomatte

Cryptomatte is a standard to efficiently create mattes for compositing.
The renderer outputs the required render passes, which can then be used
in the compositor to create masks for specified objects. Unlike the
Material and Object Index passes, the objects to isolate are selected in
compositing, and mattes will be anti-aliased.

Cryptomatte was already available in Cycles and now available in EEVEE.

Following Cycles, there are two accuracy modes. The difference between
the two modes is the number of render samples they take into account to
create the render passes. When accurate mode is off the number of levels
is used. When accuracy mode is active, the number of render samples is
used.

The settings are shared between EEVEE and Cycles.

## AOV

AOVs work in the same way as in Cycles. AOV Outputs can be defined in
the render layer tab and used in shader materials using the AOV output
node. Both Object and World based shaders are supported. The AOV can be
previewed in the viewport using the render pass selector in the shading
popover.

The settings are shared between EEVEE and Cycles. This way AOV render
passes can be viewed using material preview even when Cycles is the
active render engine.
