# Blender 2.92: Sculpting

- Sculpt: Grab silhouette option
  blender/blender@383c20a6abd.

<video src="../../../videos/2020-10-22_19-14-59.mp4" width="600" controls="" />

- Sculpt: Implement plane deformation falloff for Grab
  blender/blender@c53b6067fad.
- Sculpt: Face Set Edit delete Geometry operation
  blender/blender@c15bd1c4e65.
- Sculpt/Paint: Add Paint Studio Light preset
  blender/blender@d6180dd2f7e.
- User Interface: Add Sculpt Session info to stats
  blender/blender@ea064133e53.
- Sculpt: Allow inverting the Erase Displacement mesh filter
  blender/blender@1bc75dfa4a9.
- Sculpt: Elastic deform type for Snake Hook
  blender/blender@d870a60dd92.
- Sculpt: Multires Displacement Smear
  blender/blender@d23894d3ef3.
- Sculpt: Fair Face Sets operation for Face Set Edit
  blender/blender@c9b4d753362.
