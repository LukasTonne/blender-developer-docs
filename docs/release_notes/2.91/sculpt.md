# Blender 2.91: Sculpting

## Sculpting Workflow with multiple objects

- XYZ symmetry is now a per mesh settings which is shared between all
  painting and editing modes.
  blender/blender@5502517c3c1
- Fade Inactive Geometry Overlay that fades with the background color
  objects that are not editable in the current mode. This makes possible
  to identify which object is enabled for sculpting.
  blender/blender@ea6cd1c8f05

## Viewport

- The HDRI rotation can be locked to the view in LookDev mode to use
  EEVEE materials as Matcaps
  blender/blender@5855f317a70

<video src="../../../videos/Eevee_lock_rotation.mp4" width="600" controls="" />

- Sculpt Mode can now renders its overlays (Face Sets and Masks) in
  objects with constructive modifiers enabled.
  blender/blender@6c9ec1c893f

## Sculpt Gestures

New gesture tools to manipulate different data in the sculpt mesh.

- Box and Lasso Face Set create a new Face Set in the area affected by
  the gesture
  blender/blender@c05715b03fe.
- Box and Lasso Trim cut the mesh using a boolean operation
  blender/blender@675c9644420.
- Box mask its now its own operator, separated from selection
  blender/blender@6faa765af89.
- Sculpt: Line Project Gesture tool
  blender/blender@e0bfd3968c6.
- Sculpt: Line gestures and Mask Line tool
  blender/blender@8c81b3fb8b9.
- Sculpt: Union and Join mode for trim tools
  blender/blender@2b72860ff44.
- Sculpt: Use cursor depth in trimming gestures
  blender/blender@da7ace00d5f.

## Cloth Sculpting

- The Cloth Brush and Filter now support collisions with scene colliders
  blender/blender@675700d9489.

<video src="../../../videos/Cloth_Collisions.mp4" width="600" controls="" />

- Cloth Brush applies gravity in the entire simulated area
  blender/blender@54a2fcc0f33.
- Sculpt: Cloth Brush Soft Body Plasticity property
  blender/blender@ec9edd36ca1.

<video src="../../../videos/Plasticity.mov" width="600" controls="" />

- Sculpt: Enable persistent base for the cloth brush
  blender/blender@49c1359b556.
- New Cloth Grab Brush that uses constraints
  blender/blender@2e33c5ca15c.
- New Cloth Snake Hook Brush
  blender/blender@2e33c5ca15c.

<video src="../../../videos/Cloth_Snake_Hook.mp4" width="600" controls="" />

- Sculpt: Cloth Brush simulation area property
  blender/blender@d693d77fed9.
- Sculpt: Cloth Simulation Dynamic area mode
  blender/blender@8ef353fa506.

<video src="../../../videos/Dynamic_simulation_area.mp4" width="600" controls="" />

- Cloth brush Pin Simulation Boundary property
  blender/blender@bf65820782a.
- Sculpt: Option to limit the forces axis in the Cloth Filter
  blender/blender@4814836120e.
- Sculpt: Enable Cloth Simulation Target for Pose and Boundary
  blender/blender@c2f0522760b.
- Sculpt: Add orientation modes to the Cloth Filter
  blender/blender@b3bd121dd4c.
- Sculpt: Scale Cloth Filter
  blender/blender@5d728d38b94.

<video src="../../../videos/Simulation_Target.mp4" width="600" controls="" />

## Multires

- Multires: Base Mesh Sculpting
  blender/blender@976f0113e00.

## Tools

### Boundary Brush

This brush includes a set of deformation modes designed to deform and
control the shape of the mesh boundaries, which are really hard to do
with regular sculpt brushes (and even in edit mode). This is useful for
creating cloth assets and hard surface base meshes.
blender/blender@ed9c0464bae.
<video src="../../../videos/Boundary_Brush.mp4" width="600" controls="" />

The boundary brush supports different modes to apply the falloff which
can be used to create repeating trim patterns
blender/blender@c77bf952210.
<video src="../../../videos/Boundary_brush_falloff.mp4" width="600" controls="" />

### Multires Displacement Eraser

- This brush deletes the displacement of the Multires Modifier and moves
  the vertices towards the base mesh limit surface.
  blender/blender@478ea4c8988.

<video src="../../../videos/Displacement_eraser.mp4" width="600" controls="" />

### Sculpt Filters

- Sculpt: Sharpen Mesh Filter curvature smoothing and intensify details
  blender/blender@3570173d0f5.
- Sculpt: Sculpt Filter Orientation Options
  blender/blender@89a374cd964.
- Sculpt: Enhance Details Mesh Filter
  blender/blender@0957189d4a3.
- Sculpt: Erase Displacement Mesh Filter
  blender/blender@bedd6f90ca7.

### Other Tools

- Sculpt: Add global automasking settings support in filters
  blender/blender@6fe3521481b.

<!-- -->

- Sculpt: Invert Smooth to Enhance Details
  blender/blender@3e5431fdf43.

<video src="../../../videos/Enhance_details.mp4" width="600" controls="" />

- Sculpt: Option to mask front faces only using Lasso and Box Mask
  blender/blender@af77bf1f0f9.
- Sculpt: Option to lock the rotation in the Pose Brush scale deform
  mode
  blender/blender@9ea77f5232c.
- Sculpt: Face Set Extract Operator
  blender/blender@afb43b881cc.
- Edit Face Set is now a tool with all options exposed in the UI
  blender/blender@77e40708c2c
  blender/blender@48c0a82f7a3.
- Sculpt: Experimental Pen Tilt Support
  blender/blender@0d5ec990a98.
