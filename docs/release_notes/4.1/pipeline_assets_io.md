# Blender 4.1: Pipeline, Assets & I/O

## Stanford PLY

- PLY importer and exporter now supports custom vertex attributes
  (blender/blender@0eb6aef3b00).

## STL

- New experimental STL (.stl) exporter
  (blender/blender@17c793e43c66ba).
  The new exporter is written in C++ and is 3x-10x faster than the
  Python exporter.
