# Blender 4.1: Sculpt, Paint, Texture

- Voxel remesh attribute preservation has been changed to propagate
  *all* attributes and to improve performance
  (blender/blender@ba1c8fe6a53a00c9324607db6a1836132cef5352).
