# Blender 4.1: User Interface

## General

- Input Placeholders to show a hint about the expected value of an
  input.
  (blender/blender@b0515e34f9).

![](../../images/Placeholders.png){style="width:662px;"}

- Cryptomatte picking can now occur between separate windows.
  (blender/blender@9ee5de05c0).
- The UI interface font can now be shown in any weight.
  (blender/blender@0bde01eab5).

![](../../images/UIFontweight.png){style="width:662px;"}

- Khmer font added to support a new translation of that Cambodian
  language.
  (blender/blender@3f5654b491).
- New icons added to represent area splitting, joining, and swapping.
  (blender/blender@153dd76d22,
  blender/blender@8933284518).
- Wide Enum lists will now collapse to a single column if not enough
  space.
  (blender/blender@83ce3ef0db).
- Changing UI font in Preferences will now start in your OS Fonts
  folder.
  (blender/blender@d3a2673cb8).
- File Browser List View removes columns and reformats as width is
  decreased.
  (blender/blender@07820b0703).
- Improved Color Picker cursor indication and feedback.
  (blender/blender@c11d5b4180).

![](../../images/Colorpicker.png){style="width:532px;"}

- Outliner Context Menu "Show Hierarchy" and "Expand/Collapse All".
  (blender/blender@4793b4592b,
  blender/blender@f815484e7d).
- Text Object fonts now look in the fallback stack when characters are
  not found.
  (blender/blender@604ee2d036).

![](../../images/Textobjectfallback.png){style="width:840px;"}

- Animation marker drawing improvements.
  (blender/blender@0370feb1bf).
- Improved corner rounding for menus and popup blocks.
  (blender/blender@42ddc13033).

![](../../images/Menurounding.png){style="width:495px;"}

- Improved quality of menu and popup block shadows.
  (blender/blender@0335b6a3b7).
- Improved initial display of compositor node trees.
  (blender/blender@ff083c1595).
- New Text Objects will use translated "Text" as default.
  (blender/blender@5e38f7faf0).

![](../../images/TranslatedText.png){style="width:800px;"}

- Eyedropper can now pick colors outside the Blender window on Mac.
  (blender/blender@639de68aaa).

## Viewport

- Walk mode now supports relative up/down (using R/F keys)
  (blender/blender@c62009a6ac961e199285dee0d7b5037132a067a7).
- Improved Mesh Edge Highlighting.
  (blender/blender@dfd1b63cc7).
