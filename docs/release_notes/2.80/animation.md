# Blender 2.80: Animation & Rigging

## Keyframing & Drivers

### Drivers

- Drivers on properties can now be edited in place, without having to
  open the driver editor.
- Added an operator to delete drivers marked as "invalid".
  (blender/blender@c38ebf93e)
- Simple driver expressions are now evaluated without Python for a big
  performance improvement on complex rigs.
  (blender/blender@bf2a54b0)
- Transform Channel driver variables can now retrieve Average Scale to
  avoid the need to use three variables at once.
  (blender/blender@1854ccca)

### Animation Curves

- Automatic handle placement now takes Cyclic Extrapolation into account
  to produce a smooth loop transition.
  (blender/blender@1cb884be)
- Automatic handle placement now by default uses a new algorithm that
  produces smoother curves.
  (blender/blender@8bdc391c)

### Keyframing UI

- Dope Sheet now has options to display curve handle types and/or curve
  extreme locations via the keyframe icons.
  (blender/blender@a0dfa320)
- Keyframe insertion (both manual 'I' and Auto Keyframing) can
  optionally take cyclic extrapolation on curves into account.
  (blender/blender@7bc84559)

### NLA

- Channels affected by any of the actions in the NLA stack are now
  always set to a definite value, falling back to default when no strips
  apply.
  (blender/blender@57d4b869)
- Rigging: Custom properties now have configurable default values,
  instead of always using 0.
  (blender/blender@61c941f0)
- Inserting keyframes into strips with Add or Multiply mode correctly
  computes the key value.
  (blender/blender@de662e7c)
- Added a new Combine mode that automatically chooses
  add/multiply/quaternion math for each channel based on its type.
  (blender/blender@de1d3e5f)

## Armatures & Shape Keys

### Shape Keys

- The Radius property of Curve object vertices can now be animated via
  Shape Keys.
  (blender/blender@12788906)

### Armatures

- The Apply Pose as Rest Pose operator can now apply just the selected
  bones.
  (blender/blender@0264d839)

### Bendy Bones

- Custom handle settings have been exposed in Edit Mode to allow
  correctly drawing the rest pose.
  ([\#56268](http://developer.blender.org/T56268),
  blender/blender@61a24c79,
  blender/blender@65f77cce,
  blender/blender@748b89f1)
- A new Tangent custom handle type that uses the control bone
  orientation instead of location.
  (blender/blender@25bd9fea)
- Fixes for the Relative custom handle behavior and start handle roll.
  (blender/blender@65f77cce,
  blender/blender@3c0736bc,
  blender/blender@a33a4e13)
- B-Bones now interpolate between two closest segments when deforming
  vertices, instead of using just one.
  ([\#53700](http://developer.blender.org/T53700),
  blender/blender@51c8a6f4)
- B-Bone Scale In and Scale Out properties were split into separate X
  and Y values.
  (blender/blender@624e93bb)

## Constraints

### Disable and Keep Transform

Many constraints now have a button "Disable and Keep Transform" next to
the Influence slider. Pressing it sets the influence of this constraint
to zero while trying to maintain the object's transformation. Other
active constraints can still influence the final transformation. Note
that pressing the button and then increasing the constraint influence
may move the object; this happens when the constraint is additive rather
than replacing the transform entirely.
(blender/blender@08012ebeec7)

### Maintain Volume

- The constraint now has multiple modes for dealing with non-free axis
  scaling.
  (blender/blender@a9509a2f8,
  blender/blender@50999f7f)

### Damped Track

- The constraint is now more stable when the desired direction is
  opposite to the current one.
  ([\#55872](http://developer.blender.org/T55872),
  blender/blender@0bf80965)

### Copy Scale

- The Offset option now by default properly uses multiplication instead
  of addition.
  (blender/blender@47af343b)
- Added an option to raise the copied scale to an arbitrary power as a
  way to correctly split and invert scale.
  (blender/blender@27b9a0cd)

### Copy Transforms

- The Use B-Bone Shape option now copies the full transformation of the
  segments, instead of only location.
  (blender/blender@2aa26de3)

### Shrinkwrap

- There is a new option to specify how and when the point is moved
  towards the target surface (e.g. depending on initially being inside
  or outside the target).
  (blender/blender@3378782e)
- The Project mode now supports trying two opposite directions at once,
  and Front/Back face culling, like the modifier.
  (blender/blender@e38a0b37)
- There is a new Invert Cull option for the case when culling and two
  direction projection are used together.
  (blender/blender@e38a0b37)
- There is a new option to automatically align an axis to the (smooth)
  normal of the target.
  (blender/blender@e5b18390)
- The Above Surface option uses the smooth normal if the target mesh is
  smooth shaded.
  (blender/blender@e5b18390)
- Added a new Target Normal Project mode that is slower but much
  smoother than Nearest Surface Point.
  (blender/blender@f600b4bc)

### Armature (New)

- Added a new Armature constraint that applies the math behind the
  Armature modifier as a constraint.
  (blender/blender@798cdaee)

### Spline IK

- Added a new option to use the pre-IK Y scale of the bones to control
  their length when fitted to the curve.
  (blender/blender@ad9275ed)
- Added a new option to apply volume preservation on top of original
  scaling like in Stretch To.
  (blender/blender@37eb1090)

## Modifiers

### Shrinkwrap

- The Keep Above Surface checkbox is replaced with a new option to
  specify how and when vertices are moved towards the target surface
  (e.g. depending on initially being inside or outside the target).
  (blender/blender@3378782e)
- There is a new Invert Cull option for the case when culling and two
  direction projection are used together.
  (blender/blender@e38a0b37)
- The Above Surface option now uses the smooth normal if the target mesh
  is smooth shaded.
  (blender/blender@e5b18390)
- Added a new Target Normal Project mode that is slower but much
  smoother than Nearest Surface Point.
  (blender/blender@f600b4bc)

### Armature

- The modifier now uses the set of existing vertex group names to reduce
  the chance of spurious dependency cycles.
  (blender/blender@04708184)
