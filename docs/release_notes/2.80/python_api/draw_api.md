# Blender 2.80: Draw API

## OpenGL Core Profile

Blender now uses the OpenGL core profile, which has all the new
functionality required by EEVEE and the new viewport. However, this new
OpenGL version also removes often-used features like immediate mode and
the fixed function pipeline, make it much harder to use for simple
drawing.

We advise addons to use the *gpu* module instead of directly using
OpenGL through the *bgl* module. *gpu* provides a higher level API that
will be more future proof for upgrades or adoption of new APIs like
Vulkan.

For those addons that need more low level access or features not covered
by the *gpu* module, there are [many books and online
resources](https://www.khronos.org/opengl/wiki/Getting_Started) that
cover how to migrate to the OpenGL core profile.

## GPU module

The important new concepts for drawing are Batch and Shader objects.
Batches describe the geometry to draw, and can be efficiently reused and
cached. Shaders describe how the geometry is drawn, and can be either
builtin utility shaders or custom GLSL code. These replace immediate
mode and fixed function shading functionality that is no longer in
OpenGL core.

For details and examples, see the [GPU module API
documentation](https://docs.blender.org/api/blender2.8/gpu.html).
