# Blender 2.83: Physics

## Hair

Hair now uses the same collision engine as cloth, yielding much more
reliable collisions.
(blender/blender@d42a7bbd6ea5)

<figure>
<video src="../../../videos/Experimental_hair_collision_improvement-200554110.mp4" width="480" controls="" />
<figcaption>New hair collision example. Left is the
previous method and right is the new method.</figcaption>
</figure>

## Cloth

- Optimization for self-collision, improving overall cloth simulation
  performance by 15-20% in tests.

## Fluid

- Performance Optimizations:
  - Optimization for scenes with static (i.e. non animated) flow /
    effector objects. Baking will automatically be faster for those
    objects.
  - Improved baking speed for scenes with lots of obstacles. Effector
    objects now use the same bounding boxes as flow objects when the
    geometry is being processed.
    (blender/blender@a5c4a44df67e)
  - Loading times for particle (liquid and/or secondary particles) and
    mesh files have been improved.
    (blender/blender@7d59f84708fc,
    blender/blender@1280f3c0ae96)
- New UI Options:
  - Enable / disable effector objects during the simulation
  - Delete fluid inside obstacles
  - Subframes for obstacle objects (improves detection of fast moving
    obstacles)
    (blender/blender@cda81d5a4d39,
    blender/blender@1c3ded12f439)
- Usability:
  - Easier setup for planar emission / obstacle objects: Planes always
    have non-zero thickness and will always affect the simulation.
    (blender/blender@cda81d5a4d39)
  - Cancelling bake jobs is quicker (particularly notable when using
    domains with high resolutions)
- Important Bug Fixes:
  - Secondary (liquid) particles out of sync relative to main simulation
    ([T73988](https://developer.blender.org/T73988))
  - Secondary (liquid) particles show up in organized unrealistic
    structure ([T73552](https://developer.blender.org/T73552))
- Other:
  - Smoke rendering in 3D viewport solid mode was changed to be more
    accurate and support rendering dark smoke. This changes the look in
    existing .blend files.
  - Resolultion default was decreased along with changing the default
    cache method Replay to improve instantaneous playback
