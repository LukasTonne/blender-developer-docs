# Blender 4.0: Shading & Texturing

## Principled BSDF

The Principled BSDF node has been revamped to support a wide ranger of
material types.

- Sheen uses a new microfiber shading model, and is the top layer above
  emission and coat. Traditionally sheen has been used for fuzzy cloth,
  but now it can also be used for dust on arbitrary materials.
- Coat was placed above the emission layer. This can be used to simulate
  for example an emissive phone screen behind glass. Coat tint and IOR
  inputs were added, so that it can be used for more than a white
  clearcoat.
- Subsurface scattering uses the base color instead of a separate color
  input. There is a new subsurface scale input to adjust the RGB radius.
- Specular reflection and transmission are now primarily controlled by a
  single index of refraction input. For texturing, a new IOR Level input
  adjust the amount of specular reflection. For artistic purposes,
  specular tint be can set to an arbitrary color.
- Metallic surfaces gained edge tinting using the specular tint input.
  This is an artist friendly parameter to render complex index of
  refraction, based on the F82 tint model.
- Multiple scattering GGX is more efficient to render in Cycles, and now
  used by default. Layering of the components as a whole is more energy
  conserving, which means existing materials can render slightly darker
  or lighter depending on the scene.

See the [Principled BSDF manual page](https://docs.blender.org/manual/en/4.0/render/shader_nodes/shader/principled.html)
for details.

|Sheen to add layer of dust|Scene by Alaska using assets from Poly Haven.|
|-|-|
|![](../../images/Cycles4.0-sheen-without.jpg){style="width:400px;"}|![](../../images/Cycles4.0-sheen-with.jpg){style="width:400px;"}|

![Metallic tint from white to orange, for materials like gold.](../../images/Cycles4.0-metallic-tint.webp)

![Coat tint from white to blue, for materials like car paint.](../../images/Cycles4.0-coat-tint.webp)

## BSDF Nodes

- The Glossy and Anisotropic BSDF were merged into a single Glossy BSDF
  with anisotropy control. When opening files saved in 4.0 in older
  versions, all "Glossy BSDF" nodes will show up as Anisotropic BSDF
  nodes.
  (blender/blender@8cde7d8f8a5a07661ad614aa7716ddc1cc7c182e)
- Normal Map now has better interpolation for the Strength input for
  tangent space normal maps, avoiding the normal going below the surface
  for strength larger than 1. This changes the looks of existing renders
  where Strength was not 1. (blender/blender!109763)
- The Velvet BSDF node has been renamed to Sheen BSDF, and a new shading
  model ("Microfiber") has been added as the default option. The
  previous model is still available as "Ashikhmin", so existing files
  remain unchanged. (blender/blender!108869)
  - The new model is an implementation of ["Practical
    Multiple-Scattering Sheen Using Linearly Transformed Cosines" by
    Tizian Zeltner, Brent Burley and Matt Jen-Yuan
    Chiang](https://tizianzeltner.com/projects/Zeltner2022Practical/),
    which approximates the effect of multiple scattering between
    normal-oriented microscopic fibers using LTC distributions.
  - This BSDF is only supported in Cycles so far, in EEVEE it will be
    rendered as a diffuse BSDF.

## Texture Nodes

### Noise Texture

New inputs for the Noise Texture node, which generates fractal Perlin
noise.
(blender/blender@0702c24a36b221e8a1c3a8d660f8c9fe01d0d31e)

- Normalize: normalize output to be within 0..1 range. This is most
  useful for colors or blending factors, while bump or displacement does
  not need it.
- Lacunarity: the factor each successive layer is scaled with.

### Voronoi Texture

The Voronoi Texture node has support for fractal noise now, to add
multiple layers of details. There are 3 new inputs.
(blender/blender@144ad4d20b)

- Detail: number of layers to compute.
- Roughness: how much influence the higher layers have on the final
  output
- Lacunarity: the factor each successive layer is scaled with.

Additionally a "Normalize" property was added, which if checked will
remap the Distance and Color outputs to a \[0.0, 1.0\] range.

|Detail 1.0|2.0|4.0|
|-|-|-|
|![](../../images/F1_Detail_1.png)|![](../../images/F1_Detail_2.png)|![](../../images/F1_Detail_4.png)|

|Roughness 0.25|0.5|1.0|
|-|-|-|
|![](../../images/F1_Roughness_025.png)|![](../../images/F1_Roughness_05.png)|![](../../images/F1_Roughness_1.png)|

|Lacunarity 2.0|4.0|8.0|
|-|-|-|
|![](../../images/F1_Lacunarity_2.png)|![](../../images/F1_Lacunarity_4.png)|![](../../images/F1_Lacunarity_8.png)|

## Lights

- Light UV is now available for all light sources in Image Texture node
  (blender/blender!108691)
- Light sources have slightly different behaviours than before for
  energy preservation and better compatibility with other renders
  blender/blender#108505,
  including:
  - Point light and spot light are changed to double-sided sphere lights
    (blender/blender!108506
    blender/blender!109329).
    Besides, the light normal now points from light center to the
    sampled/intersected position on the sphere, any pipeline that was
    using the light normal should use the Incoming socket of the
    Geometry shader node instead.
  - Sun light behaves better at large angular diameters, and gives the
    same intensity regardless of the angular diameter.
    (blender/blender!108996)
  - Corrected conversion factor for area lights
    (blender/blender@482211b7e0)
