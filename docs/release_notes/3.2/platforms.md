## Windows

- Improved error message when monitors are connected to multiple
  adapters.
  (blender/blender@fd1078e1055e).
- Faster blender launch when there are disconnected network shares.
  (blender/blender@84fde382e43c).
