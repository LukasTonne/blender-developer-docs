# Core Module

## Proxy Removal

Proxies have been deprecated since Blender 3.0, but their internal
evaluation/management code was still there, and it was still possible to
keep using existing proxies defined in older versions of Blender.

They have now been fully removed. Existing proxies are converted to
library overrides as best as possible.

For more details, please see [the relevant
task](https://developer.blender.org/T91671).

## Library Overrides

### Usability

- Overridden properties are now editable in the 'Override' view,
  'Properties' view mode of the Outliner.
- The 'override hierarchy' concept (for example, an override of a full
  character is an override hierarchy, which include the root collection,
  all of its sub-collections, objects, etc.) has become much more
  important. It helps data-block relationships sane, especially when
  re-syncing after the reference library data has changed.
  - Most overrides created as part of a hierarchy are no more editable
    by default, but can be made editable from the Outliner contextual
    menu (*more ways to do so are being worked on*).

### User Interface

The UI for Library Overrides in the Outliner was reworked.

- The *Library Overrides* display mode now allows selecting between two
  view modes:
  - *Properties*: Displays all properties changed through library
    overrides, together with buttons to edit the value(s) of each
    property
    (blender/blender@d8e3bcf770c2).
    This is an improved version of the previous display mode.
    (blender/blender@76879e370245,
    blender/blender@1ff853a3f065,
    blender/blender@8b5292a60e5d)
  - *Hierarchies*: Display the hierarchical al relationships between
    overridden data-blocks. For example to override a mesh of an object
    inside a collection, Blender automatically overrides the entire
    collection -\> object -\> mesh hierarchy. A column on the right
    allows enabling/disabling overrides
    (blender/blender@994da7077d4a).
    Disabling an override will revert all overridden properties.
- Other display modes no longer list library overrides. Now that there
  is an improved display mode dedicated to library overrides, this isn't
  really needed anymore.
  (blender/blender@dcb520a7af74)

### Performance

- Performances of library override resync process were greatly improved
  (over ten times faster, depending on the file),
  blender/blender@a71a513def20,
  blender/blender@1695d38989fd.

## Movie Cache

- Fixed dead-lock in movie cache
  (blender/blender@8c4ddd5dde3)

## Objects

- New Set Parent option, `Object (Keep Transform Without Inverse)`,
  and Apply option, `Parent Inverse`
  blender/blender@db6287873cd2.
  The operators preserve the child's world transform without using the
  parent inverse matrix. Effectively, we set the child's origin to the
  parent. When the child has an identity local transform, then the child
  is world-space aligned with its parent (scale excluded).
