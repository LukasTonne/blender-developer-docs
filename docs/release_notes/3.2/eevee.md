# Eevee

- Bump node without Height plugged in is now optimized into a complete
  no-op like in Cycles to support the old workaround for defaulting a
  node group input to normal.
  (blender/blender@460d1a4cb3)
- Shader compilation progress was moved to the viewport engine info
  display. It is now a shader count instead of a progress percentage.
  (blender/blender@e2cd5c6748ff)
- Sub-Surface Scattering radius socket input is now treated as a mean
  radius instead of a scaling factor for the pre-computed SSS kernel
  (which still uses the socket default value). Manual patching of
  existing scene may be required.
  (blender/blender@80859a6cb272)
- Materials using Shader-To-RGB nodes now never receive Screen Space
  Reflections or Sub-Surface Scattering effects even if the BSDFs are
  not connected to the Shader-To-RGB nodes.
  (blender/blender@80859a6cb272)
- Displacement bump now also affects Image texture node's box projection
  mode.
  (blender/blender@80859a6cb272
  & [\#97916](http://developer.blender.org/T97916))
