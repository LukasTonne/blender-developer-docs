# Blender 2.90: Modeling

## Tools

- Extrude Manifold: new tool to automatically split and remove adjacent
  faces when extruding inwards.
  (blender/blender@b79a5bdd5ae0).
- Ruler: snapping support for ruler tool.
  (blender/blender@1f7a791)
- Topology cleanup oriented tools Merge (including By Distance aka
  Remove Doubles), Rip, Delete, Dissolve, Connect Path, Knife and Tris
  To Quads now correctly preserve custom normal vectors.
  (blender/blender@93c8955a7)
- The custom profile for the bevel modifier and tool now supports bezier
  curve handle types.
  (blender/blender@baff05ad1c15)

![Free, aligned, vector, and automatic handle types](../../images/Curve_Profile_Handles_2.90.png){style="width:600px;"}

- The bevel tool and modifier have a new 'Absolute' mode for
  interpreting the 'amount' value. This mode is like Percent, but
  measures absolute distance along adjacent edges instead of a
  percentage.
  blender/blender@466e716495ff)
- The bevel tool and modifier use a new method to decide which material
  and UV values to use for the center polygons in odd-segment bevels. It
  should give more consistency in different parts of the model compared
  to the seemingly random choice made up til now.
  blender/blender@e3a420c4773a

## UV Editor

- The new mesh editing options **Correct Face Attributes** and **Keep
  Connected** adjust UV's and vertex colors when moving mesh components.
  The option is activated in the top right corner of the 3d viewport in
  the mesh editing menu
  (blender/blender@4387aff)

![](../../images/Correct_Face_Attributes_and_Keep_Connected.png "../../images/Correct_Face_Attributes_and_Keep_Connected.png")

<video src="../../../videos/Correct_face_attributes_uv_video.mp4" width="600" controls="" />

- **Edge Ring Select**: matching edit-mesh functionality of the same
  name using `Ctrl-Alt-LeftMouse`
  (blender/blender@1dd381828fa3f).

<!-- -->

- **Pick Shortest Path**: hold down `CTRL` and select UV components
  with `left mouse` to select the shortest path between the UV
  components.
  (blender/blender@ea5fe7abc183)

<video src="../../../videos/UV_pick_shortest_path.mp4" width="600" controls="" />

- **Pick Shortest Path**: grid type selection if you activate **fill
  region** or use `Ctrl-Shift-LeftMouse` when selecting UV components

<video src="../../../videos/Pick_shortest_path_fill_region.mp4" width="600" controls="" />

- **UV Rip**: separates UV components (vertices, edges, faces) from
  connected components. The operator is run by pressing the `V`
  hotkey. The components are ripped in the direction of the mouse
  pointer position.
  (blender/blender@89cb41faa0596d898f850f68c46e2b39c25e6452)

<video src="../../../videos/UV_Rip.mp4" width="600" controls="" />

- Option to change the opacity of the UV overlay.
  (blender/blender@90d3fe1e4d).

<!-- -->

- Remove selection threshold for nearby coordinates, only select when UV
  coordinates are identical.
  (blender/blender@b88dd3b8e7b)

## Curves

- Curve Normals are disabled by default.
  (blender/blender@0bd7e202fbd6)
- Curve Handles now can be Hidden/Selected or All.
  (blender/blender@49f59092e7c8)

The new default for Handles is `Selected`. For Nurb Curves, the
handlers are displayed always as it was in previous version.

![New Handle Display](../../images/GifCurvesAll.gif){style="width:600px;"}

## Transform

- Edge and Vert Slide now supports the other snapping types.
  (blender/blender@e2fc9a88bcb3,
  blender/blender@9335daac2a36)
- The snap can now be made to the intersection between constraint and
  geometry.
  (blender/blender@d8206602feed)

![New behavior of the snap with constraint](../../images/Snap_with_Constraint.gif){style="width:600px;"}

## Modifiers

- The ocean modifier can now generate maps for spray direction.
  (blender/blender@17b89f6dacba)

![](../../images/Wave-modifier.jpg)

- Applying a modifier as a shapekey is now allowed even when the object
  has linked duplicates.
  (blender/blender@01c8aa12a1)
- New menu option to Save As Shapekey, which applies the modifier as a
  shape key without removing it.
  (blender/blender@01c8aa12a1)
