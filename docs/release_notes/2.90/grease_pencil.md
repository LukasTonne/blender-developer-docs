# Blender 2.90: Grease Pencil

## User Interface

- Onion skinning support for annotations in the video sequencer.
  (blender/blender@fda754145ae5)
- Layer Mask and Use Lights properties are animatable.
  (blender/blender@221a7002a981)
- New Material selector in Context menu and new Material menu using
  `U` key.
  (blender/blender@6a7e9f2b7647)

![Material selector context menu](../../images/Material_context_menu.png){style="width:600px;"}

- New option to put Canvas Grid in front of any object.
  (blender/blender@b369d46eb775)
- First keyframe is displayed only if the current frame is equals or
  greater
  (blender/blender@11ba9eec7017)

`This change can produce changes in old animations. Review your
drawings and set the keyframe number to be displayed at the right time.
Usually, The fix is as simple as move or duplicate the first keyframe to
frame 1.`

- Keyframe type now is accessible using python.
  (blender/blender@326db1b7cd4e)
- Default Vertex Color mode changed to `Stroke and Fill`.
  (blender/blender@e079bf6996fc)
- Added missing UV Fill property to RNA.
  (blender/blender@f382109f3885)

## Operators

- Reproject stroke now allows to keep original stroke.
  (blender/blender@37d68871b7c5)
- New Bevel Depth and Resolution parameters converting strokes to
  curves.
  (blender/blender@146473f08335)
- New Convert mesh and Bake animation to grease pencil.
  (blender/blender@bc7a4b126afb)

## Tools

- Annotate Line tool now supports different types of arrows.
  (blender/blender@668dd146f647)

![Annotate arrows types](../../images/Annotate_arrows.png){style="width:600px;"}

- Annotate tool now supports mouse stabilizer.
  (blender/blender@9a7c4e2d444f)
- New Brush options and curves to randomize effects.
  (blender/blender@b571516237a9)
- When Using the Eraser tool a new frame is created if Additive Drawing
  is enabled.
  (blender/blender@29afadcb15f7)
- Improved selection of strokes in the fill area
  (blender/blender@244ed645e08e)

![Fill Area Select](../../images/GifSelectAfter.gif){style="width:600px;"}

- Improved fill paint in vertex color mode and Tint tool.
  (blender/blender@abeda01ac6da)

## Modifiers and VFX

- New Texture Mapping modifier to manipulate UV data.
  (blender/blender@a39a6517af66)
- New Restrict Visible Points Build parameter to define the visibility
  of stroke.
  (blender/blender@a1593fa05baf)
- New Antialiasing parameter for Pixel effect.
  (blender/blender@11a390e85e2b)
