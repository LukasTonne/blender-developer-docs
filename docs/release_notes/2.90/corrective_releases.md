# Corrective Releases

## Blender 2.90.1

Released on September 23, 2020.

##### **Changelog (39)**

- Decimate Modifier: Restore vertex group factor property in UI.
  [rB97c6c4e47883](https://developer.blender.org/rB97c6c4e47883)
- GPencil Opacity modifier not working.
  [T80289](https://developer.blender.org/T80289)
- Extrude manifold can generate invalid geometry.
  [T80233](https://developer.blender.org/T80233)
- Re-ordering face maps messes up the names of other face maps.
  [T79973](https://developer.blender.org/T79973)
- Crash when deleting custom orientation.
  [T80426](https://developer.blender.org/T80426)
- Crash after duplicating and hiding vertices while using X Axis.
  [T80224](https://developer.blender.org/T80224)
- principle volume shader not working for world in Eevee.
  [T80332](https://developer.blender.org/T80332)
- Eevee OpenVDB render error when frames miss part of the grids.
  [T79718](https://developer.blender.org/T79718)
- PY API doc: fix doc for new override option of properties.
  [rB09ef19996509](https://developer.blender.org/rB09ef19996509)
- Crash when multi-mesh editing UVs with proportional editing.
  [T80561](https://developer.blender.org/T80561)
- Crash adding properties to material node-trees.
  [T80238](https://developer.blender.org/T80238)
- BLI_polyfill_calc exceeds stack size allocating points.
  [T80604](https://developer.blender.org/T80604)
- Crash deleting bone constraints when the armature layer is.
  [T80464](https://developer.blender.org/T80464)
- Cycles baking crash with locked-UI & background-mode.
  [T71012](https://developer.blender.org/T71012)
- Hook modifier crashes without vertex group data.
  [T80516](https://developer.blender.org/T80516)
- Mantaflow crash when adaptive domain + noise are enabled.
  [T79626](https://developer.blender.org/T79626)
- Mantaflow Noise Not working with Smoke/Smoke and Fire.
  [T80372](https://developer.blender.org/T80372)
- Vertex Colors not showing in edit mode.
  [T78225](https://developer.blender.org/T78225)
- Correct Face Attributes affecting modes not listed.
  [T80623](https://developer.blender.org/T80623)
- Edit Mode crash with shape keys created on blank mesh.
  [T77584](https://developer.blender.org/T77584)
- Crash accessing depsgraph from evaluated view layer.
  [T62504](https://developer.blender.org/T62504)
- Translations in python scripts are missing.
  [T80589](https://developer.blender.org/T80589)
- Crash reloading scripts from the Python console.
  [T80694](https://developer.blender.org/T80694)
- Library Override - Custom Property to Drive Child Particles results.
  [T80457](https://developer.blender.org/T80457)
- Crash on undo/ redo after changing modes.
  [T78392](https://developer.blender.org/T78392)
- UV edge select splits UV's for lasso/box/circle select.
  [T80728](https://developer.blender.org/T80728)
- potential crash in volume grids in Eevee shaders.
  [T80630](https://developer.blender.org/T80630)
- Fix OpenCL render error in large scenes.
  [rB3dbb231ed2f8](https://developer.blender.org/rB3dbb231ed2f8)
- Add versioning for 2.90 files that may have invalid mesh.
  [rB3a92a2df4519](https://developer.blender.org/rB3a92a2df4519)
- Texture paint camera project crashes after undo/redo.
  [T80885](https://developer.blender.org/T80885)
- Auto IK Double Generates IK constraints.
  [T80437](https://developer.blender.org/T80437)
- Cycles: Separate Embree device for each CPU Device.
  [rB009971ba7adc](https://developer.blender.org/rB009971ba7adc)
- Cycles crash on macOS with older CPUs.
  [T78793](https://developer.blender.org/T78793)
- Fix invert vertex group weight miscalculation for modifiers.
  [rBe0f13f41c681](https://developer.blender.org/rBe0f13f41c681)
- NLA Bake exception baking pose with non-pose selection.
  [T61985](https://developer.blender.org/T61985)
- Tris to Quads ignores UV delimit option.
  [T80520](https://developer.blender.org/T80520)
- Avoid changing the visibility of loose geometry.
  [T80771](https://developer.blender.org/T80771)
- Archipack: support for bmesh bevel arguments changes in 2.90.
  [rBA8e1b4dd71b37](https://developer.blender.org/rBA8e1b4dd71b37)
- Crash on editing multiple UVs of multiple different objects.
  [T80899](https://developer.blender.org/T80899)
